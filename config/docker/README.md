# docker-GolgiTech

Dockerfile for deploying your GolgiTech application in a Docker container, able to connect to a remote database with database-based sessions and inject ENV vars to configure your application.

Based on Ubuntu 18.04 Xenial and PHP 7.2

**Note: This project is meant to be an example to study the basics and essentials of CakePHP in a Docker environment, therefore it is build on an Ubuntu base image rather then a PHP base image, uses a 'simple' webserver like Apache and has some non-efficient commands to demonstrate stuff.**

## Usage

You can edit the `Dockerfile` to add your own git, composer or custom commands to add your application code to the image.

## Connecting to a MySQL container

Start a [MySQL container](https://hub.docker.com/_/mysql/)

```bash
docker run -d -p 9306:3306 --name mysql-server -e MYSQL_ROOT_PASSWORD=h0tsh8t -e MYSQL_DATABASE=golgi_tech mysql:5.7
```

## Running your GolgiTech docker image

Start your image forwarding container port 80 to localhost port 9000:

- Link it to the MySQL container you just started (so your container can contact it)
- Connect to a remote database server using the CakePHP DATABASE_URL env variable filled with the variables given in the command above.
- Use the `database` session handler using our the SESSION_DEFAULTS env variable (see `Dockerfile` for implementation)

# Git clone golgitech repo into ~/Documents/golgitech
- example 
```bash
git clone https://chinthakahe@bitbucket.org/mahissaraw/golgitech.git ~/Documents/golgitech

```
# copy 
```bash
cp ~/Documents/golgitech/config/app.default.php ~/Documents/golgitech/config/app.php
```

# Build the image

```bash
docker build -t gtimage ~/Documents/golgitech/config/docker

```

# Start Golgi Tech container

```bash
docker run -d -p 9000:80 --name golgi -e "DATABASE_URL=mysql://root:h0tsh8t@mysql-server/golgi_tech?encoding=utf8&timezone=Asia/Colombo&cacheMetadata=true" -e "SESSION_DEFAULTS=database" --link mysql-server:mysql -v ~/Documents/golgitech:/var/www/html gtimage
```

## Go to golgi container and run below commands

migrations command could skip for the time been

```bash
docker exec -it golgi /bin/bash
cd /var/www/html
composer update
bin/cake migrations migrate

bin/cake migrations seed
```

# In the `config/app.php`

- set fllowing database settings

'username' => 'root',
'password' => 'h0tsh8t',
'database' => 'golgi_tech'

## Test your deployment

Visit `http://localhost:9000/` in your browser or

    curl http://localhost:9000/

You can now start using your CakePHP container!

## Things to look out for

- Think about handling session when running multiple containers behind a loadbalancer. You could modify the `Dockerfile` to `sed` the `config/app.php` file to use the database or cache session handler as implemented in the example.
- If you want to store any files (e.g. uploads), please remember containers are 'stateless' and the data will be gone when you delete them. You can use [`volumes`](https://docs.docker.com/engine/tutorials/dockervolumes/#mount-a-host-directory-as-a-data-volume) or an object storage with a webservice interface like Amazon S3.
