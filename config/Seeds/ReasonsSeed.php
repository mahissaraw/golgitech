<?php
use Migrations\AbstractSeed;

/**
 * Reasons seed.
 */
class ReasonsSeed extends AbstractSeed
{
    /**
     * Run Method.
     * 
     * bin/cake migrations seed --seed ReasonsSeed
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Fungus',
				'created' => date('Y-m-d H:i:s'),
				'modified' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Bacteria',
				'created' => date('Y-m-d H:i:s'),
				'modified' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Fungus and Bacteria',
				'created' => date('Y-m-d H:i:s'),
				'modified' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Handling Errors',
				'created' => date('Y-m-d H:i:s'),
				'modified' => date('Y-m-d H:i:s')
            ]
        ];

        $table = $this->table('reasons');
        $table->insert($data)->save();
    }
}
