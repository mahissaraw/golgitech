<?php
use Migrations\AbstractSeed;

/**
 * Stages seed.
 */
class StagesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
	        [
		        'name' => 'Growth',
		        'created' => date('Y-m-d H:i:s'),
		        'modified' => date('Y-m-d H:i:s')
	        ],
	        [
		        'name' => 'Rooting',
		        'created' => date('Y-m-d H:i:s'),
		        'modified' => date('Y-m-d H:i:s')
	        ],
	        [
		        'name' => 'Multiply',
		        'created' => date('Y-m-d H:i:s'),
		        'modified' => date('Y-m-d H:i:s')
	        ],
	        [
		        'name' => 'Transfer',
		        'created' => date('Y-m-d H:i:s'),
		        'modified' => date('Y-m-d H:i:s')
	        ],
        ];

        $table = $this->table('stages');
        $table->insert($data)->save();
    }
}
