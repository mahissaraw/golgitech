<?php
use Migrations\AbstractSeed;

/**
 * MediaTypes seed.
 */
class MediaTypesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
	        [
		        'name' => 'Multiplication',
		        'created' => date('Y-m-d H:i:s'),
		        'modified' => date('Y-m-d H:i:s')
	        ],
	        [
		        'name' => 'Rooting',
		        'created' => date('Y-m-d H:i:s'),
		        'modified' => date('Y-m-d H:i:s')
	        ],
        ];

        $table = $this->table('media_types');
        $table->insert($data)->save();
    }
}
