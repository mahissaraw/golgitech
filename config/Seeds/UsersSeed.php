<?php
use Migrations\AbstractSeed;
use  Cake\Auth\DefaultPasswordHasher;

/**
 * Users seed.
 */
class UsersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $password = (new DefaultPasswordHasher)->hash('123');
        $data = [
           [
                'first_name' => 'Mahissara',
                'last_name' => 'Weerasinghe',
                'email'    => 'mahissara@tailorstore.com',
                'password' =>  $password,
                'employment_number' => 'ALA75',
                'user_type' => 'admin',
                'profile_image' => 'myAvatar.png',
                'active' => true,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            [
                'first_name' => 'Susantha',
                'last_name' => 'Bandara',
                'email'    => 'susantha@tailorstore.com',
                'password' =>  $password,
                'employment_number' => 'ALA69',
                'user_type' => 'admin',
                'profile_image' => 'myAvatar.png',
                'active' => true,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            
            [
                'first_name' => 'Chinthaka',
                'last_name' => 'Hettiarachi',
                'email'    => 'chinthaka@tailorstore.com',
                'password' =>  $password,
                'employment_number' => 'ALA70',
                'user_type' => 'admin',
                'profile_image' => 'myAvatar.png',
                'active' => true,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
        ];

        $table = $this->table('users');
        $table->insert($data)->save();
    }
}