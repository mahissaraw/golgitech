<?php
use Migrations\AbstractSeed;

/**
 * Sections seed.
 */
class SectionsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
	        [
		        'name' => 'R & D',
		        'created' => date('Y-m-d H:i:s'),
		        'modified' => date('Y-m-d H:i:s')
	        ],
	        [
		        'name' => 'Production',
		        'created' => date('Y-m-d H:i:s'),
		        'modified' => date('Y-m-d H:i:s')
	        ],
        ];

        $table = $this->table('sections');
        $table->insert($data)->save();
    }
}
