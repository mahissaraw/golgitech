<?php
use Migrations\AbstractMigration;

class CreateMedias extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('medias');
        $table->addColumn('name', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
	    $table->addColumn('media_type_id', 'integer', [
		    'default' => null,
		    'limit' => 11,
		    'null' => false,
	    ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
	    $table->addForeignKey('media_type_id', 'media_types', 'id', [
		    'delete' => 'NO_ACTION',
		    'update' => 'NO_ACTION'
        ]);
        $table->create();
    }
}