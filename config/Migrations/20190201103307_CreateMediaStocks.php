<?php
use Migrations\AbstractMigration;

class CreateMediaStocks extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('media_stocks');
        $table->addColumn('media_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('container_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
	    $table->addColumn('batch_id', 'integer', [
		    'default' => null,
		    'limit' => 11,
		    'null' => false,
	    ]);
        $table->addColumn('assigned_user_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('container_quantity', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
	    $table->addColumn('autoclave', 'integer', [
		    'default' => null,
		    'limit' => 11,
		    'null' => true,
	    ]);
        $table->addColumn('created_user_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
	    $table->addColumn('completed', 'datetime', [
		    'default' => null,
		    'null' => true,
	    ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addForeignKey('media_id', 'medias', 'id', [
            'delete' => 'NO_ACTION',
            'update' => 'NO_ACTION'
        ]);
        $table->addForeignKey('container_id', 'containers', 'id', [
            'delete' => 'NO_ACTION',
            'update' => 'NO_ACTION'
        ]);
        $table->addForeignKey('batch_id', 'batches', 'id', [
            'delete' => 'NO_ACTION',
            'update' => 'NO_ACTION'
        ]);
        $table->addForeignKey('assigned_user_id', 'users', 'id', [
            'delete' => 'NO_ACTION',
            'update' => 'NO_ACTION'
        ]);
        $table->addForeignKey('created_user_id', 'users', 'id', [
            'delete' => 'NO_ACTION',
            'update' => 'NO_ACTION'
        ]);
        $table->create();
    }
}
