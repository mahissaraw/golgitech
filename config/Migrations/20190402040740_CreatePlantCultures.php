<?php
use Migrations\AbstractMigration;

class CreatePlantCultures extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('plant_cultures');
        $table->addColumn('media_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
	    $table->addColumn('container_id', 'integer', [
		    'default' => null,
		    'limit' => 11,
		    'null' => false,
	    ]);
	    $table->addColumn('product_id', 'integer', [
		    'default' => null,
		    'limit' => 11,
		    'null' => false,
	    ]);
	    $table->addColumn('media_stock_id', 'integer', [
		    'default' => null,
		    'limit' => 11,
		    'null' => false,
	    ]);
	    $table->addColumn('section_id', 'integer', [
		    'default' => null,
		    'limit' => 11,
		    'null' => false,
	    ]);
	    $table->addColumn('stage_id', 'integer', [
		    'default' => null,
		    'limit' => 11,
		    'null' => false,
	    ]);
	    $table->addColumn('no_of_plants', 'integer', [
		    'default' => null,
		    'limit' => 11,
		    'null' => false,
	    ]);
	    $table->addColumn('label_code', 'string', [
		    'default' => null,
		    'limit' => 255,
		    'null' => true,
	    ]);
	    $table->addColumn('is_completed', 'datetime', [
		    'default' => null,
		    'null' => true,
	    ]);
	    $table->addColumn('user_id', 'integer', [
		    'default' => null,
		    'limit' => 11,
		    'null' => false,
	    ]);
	    $table->addColumn('operator_id', 'integer', [
		    'default' => null,
		    'limit' => 11,
		    'null' => false,
	    ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
	    $table->addForeignKey('media_id', 'medias', 'id', [
		    'delete' => 'NO_ACTION',
		    'update' => 'NO_ACTION'
	    ]);
	    $table->addForeignKey('container_id', 'containers', 'id', [
		    'delete' => 'NO_ACTION',
		    'update' => 'NO_ACTION'
	    ]);
	    $table->addForeignKey('product_id', 'products', 'id', [
		    'delete' => 'NO_ACTION',
		    'update' => 'NO_ACTION'
	    ]);
	    $table->addForeignKey('media_stock_id', 'media_stocks', 'id', [
		    'delete' => 'NO_ACTION',
		    'update' => 'NO_ACTION'
	    ]);
	    $table->addForeignKey('section_id', 'sections', 'id', [
		    'delete' => 'NO_ACTION',
		    'update' => 'NO_ACTION'
	    ]);
	    $table->addForeignKey('stage_id', 'stages', 'id', [
		    'delete' => 'NO_ACTION',
		    'update' => 'NO_ACTION'
	    ]);
	    $table->addForeignKey('user_id', 'users', 'id', [
		    'delete' => 'NO_ACTION',
		    'update' => 'NO_ACTION'
	    ]);
	    $table->addForeignKey('operator_id', 'users', 'id', [
		    'delete' => 'NO_ACTION',
		    'update' => 'NO_ACTION'
	    ]);
        $table->create();
    }
}
