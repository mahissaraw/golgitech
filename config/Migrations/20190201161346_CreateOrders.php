<?php
use Migrations\AbstractMigration;

class CreateOrders extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('orders');
        $table->addColumn('customer_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('order_date', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('required_date', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('shipped_date', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('order_status', 'integer', [
            'default' => null,
            'limit' => 2,
            'null' => true,
        ]);
        $table->addColumn('user_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addForeignKey('customer_id', 'customers', 'id', [
            'delete' => 'NO_ACTION',
            'update' => 'NO_ACTION'
        ]);
        $table->addForeignKey('user_id', 'users', 'id', [
            'delete' => 'NO_ACTION',
            'update' => 'NO_ACTION'
        ]);
        $table->create();
    }
}
