<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Rejections Controller
 *
 * @property \App\Model\Table\RejectionsTable $Rejections
 *
 * @method \App\Model\Entity\Rejection[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RejectionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Types']
        ];
        $rejections = $this->paginate($this->Rejections);

        $this->set(compact('rejections'));
    }

    /**
     * View method
     *
     * @param string|null $id Rejection id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $rejection = $this->Rejections->get($id, [
            'contain' => ['Types']
        ]);

        $this->set('rejection', $rejection);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $rejection = $this->Rejections->newEntity();
        if ($this->request->is('post')) {
            $rejection = $this->Rejections->patchEntity($rejection, $this->request->getData());
            if ($this->Rejections->save($rejection)) {
                $this->Flash->success(__('The rejection has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The rejection could not be saved. Please, try again.'));
        }
        $types = $this->Rejections->Types->find('list', ['limit' => 200]);
        $this->set(compact('rejection', 'types'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Rejection id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $rejection = $this->Rejections->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rejection = $this->Rejections->patchEntity($rejection, $this->request->getData());
            if ($this->Rejections->save($rejection)) {
                $this->Flash->success(__('The rejection has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The rejection could not be saved. Please, try again.'));
        }
        $types = $this->Rejections->Types->find('list', ['limit' => 200]);
        $this->set(compact('rejection', 'types'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Rejection id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $rejection = $this->Rejections->get($id);
        if ($this->Rejections->delete($rejection)) {
            $this->Flash->success(__('The rejection has been deleted.'));
        } else {
            $this->Flash->error(__('The rejection could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
