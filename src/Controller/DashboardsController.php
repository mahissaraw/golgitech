<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;


/**
 * Dashboards Controller
 *
 * @property \App\Model\Table\DashboardsTable $Dashboards
 */
class DashboardsController extends AppController
{
    public function beforeFilter(Event $event)
	{
		parent::beforeFilter($event);
		$this->Auth->allow(['index']);
	}

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */

    public function index()
    {
       if (!$this->Auth->user('id')){ return $this->redirect(['controller' => 'users','action' => 'login']); }

		//Load required models
		$this->loadModel('Users');
	    $user = $this->Users->findById($this->Auth->user('id'), [
		    'contain' => []
	    ])->firstOrFail();
	    $this->set('user', $user);

    }

	/**
	 * MAINTENANCE
	 *  - uncomment the line in config/routes.php file
	 *  - when maintenance
	 */
	public function maintenance()
	{
		$this->layout = false;
	}

}