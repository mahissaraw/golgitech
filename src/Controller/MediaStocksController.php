<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * MediaStocks Controller
 *
 * @property \App\Model\Table\MediaStocksTable $MediaStocks
 *
 * @method \App\Model\Entity\MediaStock[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MediaStocksController extends AppController
{

    public function initialize(){
        parent::initialize();
        $this->loadcomponent('QrCode');
    }


    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Medias', 'Containers', 'Batches', 'Users', 'CreatedUsers']
        ];
        $mediaStocks = $this->paginate($this->MediaStocks);

        $this->set(compact('mediaStocks'));
    }

    /**
     * View method
     *
     * @param string|null $id Media Stock id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $mediaStock = $this->MediaStocks->get($id, [
            'contain' => ['Medias', 'Containers', 'Batches', 'Users', 'CreatedUsers']
        ]);

        $this->set('mediaStock', $mediaStock);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $mediaStock = $this->MediaStocks->newEntity();
        if ($this->request->is('post')) {
            $batches = $this->loadModel('Batches');
            $mediaStock = $this->MediaStocks->patchEntity($mediaStock, $this->request->getData());
            $newBatch = $batches->newEntity();
            $newBatch->name = 'aaaaaaaa'; #Todo: User proper name
            if ($batches->save($newBatch)) {
                $batchId = $newBatch->id;
            }
            $mediaStock->batch_id = $batchId;
            $mediaStock->created_user_id = $this->Auth->user('id');
            if ($this->MediaStocks->save($mediaStock)) {
                $this->Flash->success(__('The media stock has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The media stock could not be saved. Please, try again.'));
        }
        $medias = $this->MediaStocks->Medias->find('list',[
		    'keyField'      => 'id',
		    'valueField'    => 'name_with_type'
	        ])
	        ->order(['Medias.media_type_id' => 'ASC']);
        $containers = $this->MediaStocks->Containers->find('list', [
            'keyField'      => 'id',
            'valueField'    => 'name_with_type'
            ])
            ->order(['Containers.media_type_id' => 'ASC']);
        $users = $this->MediaStocks->Users->find('list', ['limit' => 200]);
        $this->set(compact('mediaStock', 'medias', 'containers', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Media Stock id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $mediaStock = $this->MediaStocks->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $mediaStock = $this->MediaStocks->patchEntity($mediaStock, $this->request->getData());
            if ($this->MediaStocks->save($mediaStock)) {
                $this->Flash->success(__('The media stock has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The media stock could not be saved. Please, try again.'));
        }
        $medias = $this->MediaStocks->Medias->find('list',[
		    'keyField'      => 'id',
		    'valueField'    => 'name_with_type'
	        ])
	        ->order(['Medias.media_type_id' => 'ASC']);
        $containers = $this->MediaStocks->Containers->find('list', [
            'keyField'      => 'id',
            'valueField'    => 'name_with_type'
            ])
            ->order(['Containers.media_type_id' => 'ASC']);
        $batches = $this->MediaStocks->Batches->find('list', ['limit' => 200]);
        $users = $this->MediaStocks->Users->find('list', ['limit' => 200]);
        $this->set(compact('mediaStock', 'medias', 'containers', 'batches', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Media Stock id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $mediaStock = $this->MediaStocks->get($id);
        if ($this->MediaStocks->delete($mediaStock)) {
            $this->Flash->success(__('The media stock has been deleted.'));
        } else {
            $this->Flash->error(__('The media stock could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Generates the qrcode label
     * 
     * @param string|null $id
     * @return \Cake\Http\Response|null Redirects to index.
     */
    public function label($id = null)
    {
        if ($id) {

	        $mediaStock = $this->MediaStocks->get($id, [
		        'contain' => ['Medias', 'Containers', 'Batches', 'Users', 'CreatedUsers']
	        ]);

	        if (Configure::read('debug')){
	        	$url = Configure::read('host_address');
	        } else {
		        $url = 'http://'.$_SERVER['REMOTE_ADDR'];
	        }

	        try{
	            $this->QrCode->generateQrCode($mediaStock->id, $url.'/media-stocks/view/');
	        } catch (\Exception $e){
		        echo $e->getMessage();
	        }

	        $this->set('id', $mediaStock->id);
	        $this->set('created_year', date('Y', strtotime($mediaStock->created)));
	        $this->set('created_week', date('W', strtotime($mediaStock->created)));

	        $base_path = 'img' . DS . 'qr' . DS . $id . '.pdf';
	        $save_path = ROOT . DS . 'webroot' . DS . $base_path;

	        $CakePdf = new \CakePdf\Pdf\CakePdf();
	        $CakePdf->template('label', 'label');
	        $CakePdf->viewVars($this->viewVars);

	        $pdf = $CakePdf->write($save_path);

	        if ($pdf) {
		        # ToDo: Print multiple labels at once -n#
		        $printCmd = "lp -d golgi-label-printer -o fit-to-page ";
		        //$printCmd .= "-o media=Custom.4x1in ";
		        $printCmd .= $save_path;
		        exec($printCmd, $output, $return_var);

                if (!$return_var) {
                    $response = [
                        'success' => 1,
                        'message' => 'Label generated and printed successfully',
                        'id' => $id,
                        'alert' => 'alert-info'
                    ];
                } else {
                    $response = [
                        'success' => 2,
                        'message' => 'Label generated successfully',
                        'id' => $id,
                        'alert' => 'alert-warning'
                    ];
                }

	        } else {
		        $response = [
			        'success' => 0,
			        'message' => 'Could not generate label',
			        'id' => $id,
                    'alert' => 'alert-danger'
		        ];
	        }

	        $this->set('data', $response);
	        $this->render('/Common/json', 'ajax/ajax');
        }

    }

    public function execInBackground($cmd) {
        $base_path = 'img' . DS . 'qr' . DS . $cmd . '.pdf';
        $save_path = ROOT . DS . 'webroot' . DS . $base_path;

        $printCmd = "lp -d golgi-label-printer -o fit-to-page ";
        $printCmd .= $save_path;
        exec($printCmd, $output, $return_var);

        debug($printCmd);
        debug($output);
        debug($return_var);
        if (!$return_var) {
            echo "PDF Printed Successfully";
        } else {
            echo "PDF NOT Printed";
        }
        die();

    }
}