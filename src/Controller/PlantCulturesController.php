<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * PlantCultures Controller
 *
 * @property \App\Model\Table\PlantCulturesTable $PlantCultures
 *
 * @method \App\Model\Entity\PlantCulture[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PlantCulturesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Medias', 'Containers', 'Products', 'MediaStocks', 'Sections', 'Stages', 'Users']
        ];
        $plantCultures = $this->paginate($this->PlantCultures);
        $reasons = TableRegistry::getTableLocator()->get('Reasons')->find();

        $this->set(compact('plantCultures','reasons'));
    }

    /**
     * View method
     *
     * @param string|null $id Plant Culture id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $plantCulture = $this->PlantCultures->get($id, [
            'contain' => ['Medias', 'Containers', 'Products', 'MediaStocks', 'Sections', 'Stages', 'Users']
        ]);

        $this->set('plantCulture', $plantCulture);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $plantCulture = $this->PlantCultures->newEntity();
        if ($this->request->is('post')) {
            $plantCulture = $this->PlantCultures->patchEntity($plantCulture, $this->request->getData());
            $plantCulture->user_id = $this->getRequest()->getSession()->read('Auth.User.id');

            if ($this->PlantCultures->save($plantCulture)) {
                $this->Flash->success(__('The plant culture has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The plant culture could not be saved. Please, try again.'));
        }
        $medias = $this->PlantCultures->Medias->find('list', ['limit' => 200]);
        $containers = $this->PlantCultures->Containers->find('list', ['limit' => 200]);
        $products = $this->PlantCultures->Products->find('list', ['limit' => 200]);
        $mediaStocks = $this->PlantCultures->MediaStocks->find('list', ['limit' => 200]);
        $sections = $this->PlantCultures->Sections->find('list', ['limit' => 200]);
        $stages = $this->PlantCultures->Stages->find('list', ['limit' => 200]);
        $users = $this->PlantCultures->Users->find('list', ['limit' => 200]);
        $this->set(compact('plantCulture', 'medias', 'containers', 'products', 'mediaStocks', 'sections', 'stages', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Plant Culture id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $plantCulture = $this->PlantCultures->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $plantCulture = $this->PlantCultures->patchEntity($plantCulture, $this->request->getData());
            if ($this->PlantCultures->save($plantCulture)) {
                $this->Flash->success(__('The plant culture has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The plant culture could not be saved. Please, try again.'));
        }
        $medias = $this->PlantCultures->Medias->find('list', ['limit' => 200]);
        $containers = $this->PlantCultures->Containers->find('list', ['limit' => 200]);
        $products = $this->PlantCultures->Products->find('list', ['limit' => 200]);
        $mediaStocks = $this->PlantCultures->MediaStocks->find('list', ['limit' => 200]);
        $sections = $this->PlantCultures->Sections->find('list', ['limit' => 200]);
        $stages = $this->PlantCultures->Stages->find('list', ['limit' => 200]);
        $users = $this->PlantCultures->Users->find('list', ['limit' => 200]);
        $this->set(compact('plantCulture', 'medias', 'containers', 'products', 'mediaStocks', 'sections', 'stages', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Plant Culture id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $plantCulture = $this->PlantCultures->get($id);
        if ($this->PlantCultures->delete($plantCulture)) {
            $this->Flash->success(__('The plant culture has been deleted.'));
        } else {
            $this->Flash->error(__('The plant culture could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function transferGrowthRoom($id = null)
    {
        $plantCulture = $this->PlantCultures->get($id, [
            'contain' => []
        ]);

        $stages = $this->PlantCultures->Stages->findByName('Growth')->first();

        if ($this->request->is(['patch', 'post', 'put'])) {
            $plantCulture = $this->PlantCultures->patchEntity($plantCulture, $this->request->getData());
            $plantCulture->stage_id = $stages->id;
            if ($this->PlantCultures->save($plantCulture)) {
                $this->Flash->success(__('Successfully transferred to growth room.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The plant culture could not be saved. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
