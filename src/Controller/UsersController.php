<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
	public function initialize()
	{
		parent::initialize();
		$this->Auth->allow(['logout', 'login']);
	}

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData(), ['validate' => 'passwordConfirm']);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

	/**
	 * Custom login function for AuthComponent
	 *
	 * @return \Cake\Network\Response|null
	 */
	public function login(){
		if ($this->request->is('post')) {
            $user = $this->Auth->identify();
			if ($user && $user['active'] === true) {
				$this->Auth->setUser($user);
				return $this->redirect($this->Auth->redirectUrl());
			}
			$this->Flash->error('Your username or password is incorrect or inactive account.');
		}
		$this->viewBuilder()->setLayout('login');
	}

	/**
	 * Logout user from the system
	 *
	 * @return \Cake\Network\Response|null
	 */
	public function logout(){
		$this->Flash->success('You are now logged out.');
		return $this->redirect($this->Auth->logout());
	}

	/**
	 * Method for administrators to log in as users using their credentials
	 * @param $id
	 */
	public function adminLoginUser($id) {

		if (!$this->Users->exists($id)) {
			throw new NotFoundException(__('Invalid User'));
		}

		$user = $this->Users->findById($id)->firstOrFail();
		$this->getRequest()->getSession()->write('Auth.Admin', $this->getRequest()->getSession()->read('Auth.User'));
		$this->getRequest()->getSession()->delete('Auth.User');
		$this->Auth->setUser($user->toArray());
		$this->redirect($this->Auth->redirectUrl());
		$this->autoRender = false;
	}

	/**
	 * method for login back as admin
	 *
	 */
	public function adminLogoutUser() {
		if ($this->getRequest()->getSession()->read('Auth.Admin')) {
			$this->Auth->setUser($this->getRequest()->getSession()->read('Auth.Admin'));
			$this->getRequest()->getSession()->delete('Auth.Admin');
			$this->redirect($this->Auth->redirectUrl());
		}
		$this->autoRender = false;
	}
}