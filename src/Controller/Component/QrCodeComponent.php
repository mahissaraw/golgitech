<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Endroid\QrCode\QrCode;


/**
 * QrCode component
 */
class QrCodeComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    /**
     * Generate qr code image (png)
     * To generate qrCode we use endroid/qr-code library
     * $this->loadcomponent('QrCode');
     * @param integer $id
     * @param string $path
     * @return string
     *
     */
    public function generateQrCode($id, $path = null){
        $base_path      = 'img'.DS.'qr'.DS.$id.'.png';
        $save_path      = ROOT.DS.'webroot'.DS.$base_path;

        $qr_data = $path.$id;

        $qrCode = new QrCode($qr_data);
		$qrCode->setSize(900);
        $qrCode->setMargin(10);
        $qrCode->writeFile($save_path);
        return $qr_data;
    }
}