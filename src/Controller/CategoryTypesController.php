<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CategoryTypes Controller
 *
 * @property \App\Model\Table\CategoryTypesTable $CategoryTypes
 *
 * @method \App\Model\Entity\CategoryType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CategoryTypesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $categoryTypes = $this->paginate($this->CategoryTypes);

        $this->set(compact('categoryTypes'));
    }

    /**
     * View method
     *
     * @param string|null $id Category Type id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $categoryType = $this->CategoryTypes->get($id, [
            'contain' => ['Categories']
        ]);

        $this->set('categoryType', $categoryType);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $categoryType = $this->CategoryTypes->newEntity();
        if ($this->request->is('post')) {
            $categoryType = $this->CategoryTypes->patchEntity($categoryType, $this->request->getData());
            if ($this->CategoryTypes->save($categoryType)) {
                $this->Flash->success(__('The category type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The category type could not be saved. Please, try again.'));
        }
        $this->set(compact('categoryType'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Category Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $categoryType = $this->CategoryTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $categoryType = $this->CategoryTypes->patchEntity($categoryType, $this->request->getData());
            if ($this->CategoryTypes->save($categoryType)) {
                $this->Flash->success(__('The category type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The category type could not be saved. Please, try again.'));
        }
        $this->set(compact('categoryType'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Category Type id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $categoryType = $this->CategoryTypes->get($id);
        if ($this->CategoryTypes->delete($categoryType)) {
            $this->Flash->success(__('The category type has been deleted.'));
        } else {
            $this->Flash->error(__('The category type could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
