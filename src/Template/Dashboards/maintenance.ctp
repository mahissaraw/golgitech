<title>
	<?php echo 'Maintenance'; ?>
</title>
<div class="text-center">
    <img class="responsive-img"
         src="/img/golgi_tec_logo.png" style="width: 500px;"/>
    <br/>
    <img class="responsive-img" style="width: 100px;"
         src="/img/maintenance.png" />
</div>
<article>
    <h1>We&rsquo;ll be back soon!</h1>
    <div>
        <p>Sorry for the inconvenience, we&rsquo;re performing some <span style="color:orangered">maintenance</span> at the moment. we&rsquo;ll be back online shortly!</p>

        <p>&mdash; IT Team -</p>
    </div>
</article>

<form>
    <input type="button" value="Home Page" onclick="window.location.href='/'" />
</form>
<style>
    body { text-align: center; padding: 150px; }
    h1 { font-size: 50px; }
    body { font: 20px Helvetica, sans-serif; color: #333; }
    article { display: block; text-align: left; width: 650px; margin: 0 auto; }
    input[type=button]:hover {
        background-color: #343a40;
        border-color: #343a40;
    }
    input[type=button]{
        color: #fff;
        background-color: #6c757d;
        border-color: #6c757d;
        padding: .4rem .75rem;
        border-radius: 4px;
        cursor: pointer;
    }
</style>