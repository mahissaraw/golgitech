<div class="media border p-3">
	<?php
	if ($user->profile_image){
		$image = $user->profile_image;
	} else {
		$image = "user.png";
	}
	echo $this->Html->image("/files/Users/profile_image/".$image, [
		'class' => 'mr-3 mt-3 rounded-circle',
		'width' => '60px'
	]); ?>
    <div class="media-body">
        <h4><?= $user->full_name ?> <small><i><?= "Today is " . date("Y-m-d") ?></i></small></h4>
        <p><?= $user->user_type ?></p>
    </div>
</div>
<!--<div class="container" style="margin-top: 20px;">-->
    <div class="card-deck" style="margin-top: 20px;">
        <div class="card" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
        </div>
        <div class="card" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
        </div>
        <div class="card" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
        </div>
    </div>
<!--    <div class="card">-->
<!--        <h5 class="card-header">Featured</h5>-->
<!--        <div class="card-body">-->
<!--            <h5 class="card-title">Special title treatment</h5>-->
<!--            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>-->
<!--            <a href="#" class="btn btn-primary">Go somewhere</a>-->
<!--        </div>-->
<!--    </div>-->
    </div>
<!--</div>-->