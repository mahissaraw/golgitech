<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PlantCulture $plantCulture
 */
?>
<div class='dropdown'>
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right btn-sm" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $plantCulture->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $plantCulture->id), 'class' => 'dropdown-item']
            )
            ?></li>
        <li><?= $this->Html->link(__('List Plant Cultures'), ['action' => 'index'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Medias'), ['controller' => 'Medias', 'action' => 'index'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New Media'), ['controller' => 'Medias', 'action' => 'add'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Containers'), ['controller' => 'Containers', 'action' => 'index'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New Container'), ['controller' => 'Containers', 'action' => 'add'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Media Stocks'), ['controller' => 'MediaStocks', 'action' => 'index'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New Media Stock'), ['controller' => 'MediaStocks', 'action' => 'add'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Sections'), ['controller' => 'Sections', 'action' => 'index'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New Section'), ['controller' => 'Sections', 'action' => 'add'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Stages'), ['controller' => 'Stages', 'action' => 'index'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New Stage'), ['controller' => 'Stages', 'action' => 'add'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add'], ['class' => "dropdown-item"]) ?></li>
    </ul>
</div>
<div class="plantCultures form large-9 medium-8 columns content">
    <?= $this->Form->create($plantCulture) ?>
    <fieldset>
        <legend><?= __('Edit Plant Culture') ?></legend>
        <div class="form-group">
            <?= $this->Form->control('media_id', ['options' => $medias, 'class' => 'form-control']); ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('container_id', ['options' => $containers, 'class' => 'form-control']); ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('product_id', ['options' => $products, 'class' => 'form-control']); ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('media_stock_id', ['options' => $mediaStocks, 'class' => 'form-control']); ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('section_id', ['options' => $sections, 'class' => 'form-control']); ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('stage_id', ['options' => $stages, 'class' => 'form-control']); ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('no_of_plants', ['class' => 'form-control']); ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('label_code', ['class' => 'form-control']); ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control(
                'is_completed',
                [
                    'label' => __('Is Completed'),
                    'type'=>'datetime',
                    'id' => 'datetimepicker',
                    'class' => 'form-control'
                ]
            ); ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('user_id', ['class' => 'form-control']); ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('operator_id', ['options' => $users, 'class' => 'form-control']); ?>
        </div>
    </fieldset>
    <?= $this->Form->button(__('Submit'),['class' => 'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
</div>

