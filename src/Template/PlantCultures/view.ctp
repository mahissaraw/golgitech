<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PlantCulture $plantCulture
 */
?>
<div class='dropdown'>
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right btn-sm" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><?= $this->Html->link(__('Edit Plant Culture'), ['action' => 'edit', $plantCulture->id], ['class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Plant Culture'), ['action' => 'delete', $plantCulture->id], ['confirm' => __('Are you sure you want to delete # {0}?', $plantCulture->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Plant Cultures'), ['action' => 'index'], ['class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('New Plant Culture'), ['action' => 'add'], ['class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('List Medias'), ['controller' => 'Medias', 'action' => 'index'], ['class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('New Media'), ['controller' => 'Medias', 'action' => 'add'], ['class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('List Containers'), ['controller' => 'Containers', 'action' => 'index'], ['class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('New Container'), ['controller' => 'Containers', 'action' => 'add'], ['class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index'], ['class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add'], ['class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('List Media Stocks'), ['controller' => 'MediaStocks', 'action' => 'index'], ['class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('New Media Stock'), ['controller' => 'MediaStocks', 'action' => 'add'], ['class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('List Sections'), ['controller' => 'Sections', 'action' => 'index'], ['class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('New Section'), ['controller' => 'Sections', 'action' => 'add'], ['class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('List Stages'), ['controller' => 'Stages', 'action' => 'index'], ['class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('New Stage'), ['controller' => 'Stages', 'action' => 'add'], ['class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index'], ['class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add'], ['class' => "dropdown-item"]) ?> </li>
    </ul>
</div>
<div class="plantCultures view large-9 medium-8 columns content">
    <h3><?= h($plantCulture->id) ?></h3>
    <table class="table">
        <tr>
            <th scope="row"><?= __('Media') ?></th>
            <td><?= $plantCulture->has('media') ? $this->Html->link($plantCulture->media->name, ['controller' => 'Medias', 'action' => 'view', $plantCulture->media->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Container') ?></th>
            <td><?= $plantCulture->has('container') ? $this->Html->link($plantCulture->container->name, ['controller' => 'Containers', 'action' => 'view', $plantCulture->container->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Product') ?></th>
            <td><?= $plantCulture->has('product') ? $this->Html->link($plantCulture->product->name, ['controller' => 'Products', 'action' => 'view', $plantCulture->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Media Stock') ?></th>
            <td><?= $plantCulture->has('media_stock') ? $this->Html->link($plantCulture->media_stock->id, ['controller' => 'MediaStocks', 'action' => 'view', $plantCulture->media_stock->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Section') ?></th>
            <td><?= $plantCulture->has('section') ? $this->Html->link($plantCulture->section->name, ['controller' => 'Sections', 'action' => 'view', $plantCulture->section->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Stage') ?></th>
            <td><?= $plantCulture->has('stage') ? $this->Html->link($plantCulture->stage->name, ['controller' => 'Stages', 'action' => 'view', $plantCulture->stage->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Label Code') ?></th>
            <td><?= h($plantCulture->label_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $plantCulture->has('user') ? $this->Html->link($plantCulture->user->full_name, ['controller' => 'Users', 'action' => 'view', $plantCulture->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($plantCulture->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('No Of Plants') ?></th>
            <td><?= $this->Number->format($plantCulture->no_of_plants) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Id') ?></th>
            <td><?= $this->Number->format($plantCulture->user_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Completed') ?></th>
            <td><?= h($plantCulture->is_completed) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($plantCulture->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($plantCulture->modified) ?></td>
        </tr>
    </table>
</div>
