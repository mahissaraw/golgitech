<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PlantCulture[]|\Cake\Collection\CollectionInterface $plantCultures
 */
?>
<div class='dropdown'>
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right btn-sm" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><?= $this->Html->link(__('New Plant Culture'), ['action' => 'add'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Medias'), ['controller' => 'Medias', 'action' => 'index'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New Media'), ['controller' => 'Medias', 'action' => 'add'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Containers'), ['controller' => 'Containers', 'action' => 'index'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New Container'), ['controller' => 'Containers', 'action' => 'add'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Media Stocks'), ['controller' => 'MediaStocks', 'action' => 'index'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New Media Stock'), ['controller' => 'MediaStocks', 'action' => 'add'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Sections'), ['controller' => 'Sections', 'action' => 'index'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New Section'), ['controller' => 'Sections', 'action' => 'add'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Stages'), ['controller' => 'Stages', 'action' => 'index'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New Stage'), ['controller' => 'Stages', 'action' => 'add'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add'], ['class' => "dropdown-item"]) ?></li>
    </ul>
</div>
<div class="customers index large-9 medium-8 columns content">
    <h3><?= __('Plant Cultures') ?></h3>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('media_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('container_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('product_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('media_stock_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('section_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('stage_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('no_of_plants') ?></th>
                <th scope="col"><?= $this->Paginator->sort('label_code') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('operator_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($plantCultures as $plantCulture): ?>
            <tr>
                <td><?= $this->Number->format($plantCulture->id) ?></td>
                <td><?= $plantCulture->has('media') ? $this->Html->link($plantCulture->media->name, ['controller' => 'Medias', 'action' => 'view', $plantCulture->media->id]) : '' ?></td>
                <td><?= $plantCulture->has('container') ? $this->Html->link($plantCulture->container->name, ['controller' => 'Containers', 'action' => 'view', $plantCulture->container->id]) : '' ?></td>
                <td><?= $plantCulture->has('product') ? $this->Html->link($plantCulture->product->name, ['controller' => 'Products', 'action' => 'view', $plantCulture->product->id]) : '' ?></td>
                <td><?= $plantCulture->has('media_stock') ? $this->Html->link($plantCulture->media_stock->id, ['controller' => 'MediaStocks', 'action' => 'view', $plantCulture->media_stock->id]) : '' ?></td>
                <td><?= $plantCulture->has('section') ? $this->Html->link($plantCulture->section->name, ['controller' => 'Sections', 'action' => 'view', $plantCulture->section->id]) : '' ?></td>
                <td><?= $plantCulture->has('stage') ? $this->Html->link($plantCulture->stage->name, ['controller' => 'Stages', 'action' => 'view', $plantCulture->stage->id]) : '' ?></td>
                <td><?= $this->Number->format($plantCulture->no_of_plants) ?></td>
                <td><?= h($plantCulture->label_code) ?></td>
                <td><?= $this->Number->format($plantCulture->user_id) ?></td>
                <td><?= $plantCulture->has('user') ? $this->Html->link($plantCulture->user->full_name, ['controller' => 'Users', 'action' => 'view', $plantCulture->user->id]) : '' ?></td>
                <td class="actions">
                    <div class="btn-group btn-group-toggle">
                        <?= $this->Html->link(__('Reject'),['action' => '#'], ['data-toggle'=>"modal", 'data-target'=>"#rejectModal", 'class'=> 'btn btn-danger btn_remove']) ?>
                        <?= $this->Html->link(__('Multiply'), ['action' => 'view', $plantCulture->id], ['class'=> 'btn btn-info']) ?>
                        <?= $this->Form->postLink(__('Return Growth Room'), ['action' => 'transferGrowthRoom', $plantCulture->id], ['class'=> 'btn btn-secondary']) ?>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

<!-- The Reject Modal -->
<div class="modal" id="rejectModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Reject Reasons</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <?php foreach ($reasons as $reason){ ?>
                    <div class="checkbox">
                        <label><input type="checkbox" value="<?= $reason->id ?>"> <?= $reason->name?></label>
                    </div>
                <?php } ?>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary float-right">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

