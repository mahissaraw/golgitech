<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Rejection[]|\Cake\Collection\CollectionInterface $rejections
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Rejection'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="rejections index large-9 medium-8 columns content">
    <h3><?= __('Rejections') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('type_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('amount') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($rejections as $rejection): ?>
            <tr>
                <td><?= $this->Number->format($rejection->id) ?></td>
                <td><?= h($rejection->type) ?></td>
                <td><?= $this->Number->format($rejection->type_id) ?></td>
                <td><?= $this->Number->format($rejection->amount) ?></td>
                <td><?= h($rejection->created) ?></td>
                <td><?= h($rejection->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $rejection->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $rejection->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $rejection->id], ['confirm' => __('Are you sure you want to delete # {0}?', $rejection->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
