<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Rejection $rejection
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Rejections'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="rejections form large-9 medium-8 columns content">
    <?= $this->Form->create($rejection) ?>
    <fieldset>
        <legend><?= __('Add Rejection') ?></legend>
        <?php
            echo $this->Form->control('type');
            echo $this->Form->control('type_id');
            echo $this->Form->control('reasons');
            echo $this->Form->control('amount');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
