<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Rejection $rejection
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Rejection'), ['action' => 'edit', $rejection->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Rejection'), ['action' => 'delete', $rejection->id], ['confirm' => __('Are you sure you want to delete # {0}?', $rejection->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Rejections'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Rejection'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="rejections view large-9 medium-8 columns content">
    <h3><?= h($rejection->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Type') ?></th>
            <td><?= h($rejection->type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($rejection->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Type Id') ?></th>
            <td><?= $this->Number->format($rejection->type_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Amount') ?></th>
            <td><?= $this->Number->format($rejection->amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($rejection->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($rejection->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Reasons') ?></h4>
        <?= $this->Text->autoParagraph(h($rejection->reasons)); ?>
    </div>
</div>
