<?php $controller = $this->request->getParam('controller'); ?>
<?php $pass = $this->request->getParam('pass');?>
<div id="sidebar-wrapper">
    <ul class="sidebar navbar-nav">
        <li
            class="nav-item <?= (
                $controller === '' ||
                $controller === 'Dashboards'
                ) ? 'active': ''?>">
            <a class="nav-link" href="/dashboards">
                <i class="fas fa-home"></i>
                <span>Dashboard</span>
            </a>
        </li>
        <li class="nav-item <?= $controller === 'Users' ? 'active': ''?>">
            <a class="nav-link" href="/users">
                <i class="fas fa-users"></i>
                <span>Users</span></a>
        </li>
        <li class="nav-item <?= $controller === 'MediaStocks' ? 'active': ''?>">
            <a class="nav-link" href="/media-stocks">
                <i class="fas fa-stream"></i>
                <span>Media Stocks</span></a>
        </li>
        <li
                class="nav-item dropdown <?= ($controller === 'PlantCultures' && $pass[0] == 'rd')  ? 'active': ''?>">
            <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-stream"></i>
                <span>R & D</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                <a class="dropdown-item <?= ($controller === 'PlantCultures' && $pass[1] == 'growth') ? 'active': ''?>" href="/plant-cultures/index/rd/growth">Growth Room</a>
                <a class="dropdown-item <?= ($controller === 'PlantCultures' && $pass[1] == 'transfer') ? 'active': ''?>" href="/plant-cultures/index/rd/transfer">Transfer Room</a>
            </div>
        </li>
        <li
                class="nav-item dropdown <?= (
                    $controller === 'Medias' ||
                    $controller === 'MediaTypes'
                ) ? 'active': ''?>">
            <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-stream"></i>
                <span>Production</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                <a class="dropdown-item <?= $controller === 'Medias' ? 'active': ''?>" href="/medias">Growth Room</a>
                <a class="dropdown-item <?= $controller === 'MediaTypes' ? 'active': ''?>" href="/media-types">Transfer Room</a>
                <a class="dropdown-item <?= $controller === 'MediaTypes' ? 'active': ''?>" href="/media-types">Rooting Room</a>
            </div>
        </li>
        <li
            class="nav-item dropdown <?= (
                $controller === 'Medias' ||
                $controller === 'MediaTypes' ||
                $controller === 'Containers' ||
                $controller === 'Categories' ||
                $controller === 'Stages' ||
                $controller === 'Sections' ||
                $controller === 'Reasons' ||
                $controller === 'Rejections' ||
                $controller === 'Products'
        ) ? 'active': ''?>">
            <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-tools"></i>
                <span>Settings</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                <a class="dropdown-item <?= $controller === 'Products' ? 'active': ''?>"
                   href="/products">Products</a>
                <a class="dropdown-item <?= $controller === 'Medias' ? 'active': ''?>" href="/medias">Medias</a>
                <a class="dropdown-item <?= $controller === 'MediaTypes' ? 'active': ''?>" href="/media-types">Media
                    Types</a>
                <a class="dropdown-item <?= $controller === 'Containers' ? 'active': ''?>"
                    href="/containers">Containers</a>
                <a class="dropdown-item <?= $controller === 'Categories' ? 'active': ''?>"
                    href="/categories">Categories</a>
                <a class="dropdown-item <?= $controller === 'CategoryTypes' ? 'active': ''?>"
                    href="/category-types">Category Types</a>
                <div class="dropdown-divider"></div>
                <h6 class="dropdown-header">Other Tables:</h6>
                <a class="dropdown-item <?= $controller === 'Stages' ? 'active': ''?>" href="/stages">Stages</a>
                <a class="dropdown-item <?= $controller === 'Sections' ? 'active': ''?>" href="/sections">Sections</a>
                <a class="dropdown-item <?= $controller === 'Reasons' ? 'active': ''?>" href="/reasons">Reasons</a>
            </div>
        </li>
        <?php if ($loggedUser->isAuthorized(['admin', 'manager', 'supervisor'])) :?>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <i class="fas fa-chart-line"></i>
                <span>Reports</span></a>
        </li>
        <?php endif; ?>
    </ul>
</div>
<script>
$("#sidebarToggle").on('click', function(e) {
    e.preventDefault();
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");
});
</script>