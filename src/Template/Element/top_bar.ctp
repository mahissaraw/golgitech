<?php $session = $this->getRequest()->getSession(); ?>
<nav class="navbar navbar-expand navbar-dark bg-dark static-top ml-auto mr-0 mr-md-3 my-md-0">
	<a class="navbar-brand mr-1" href="#">GolgiTec</a>
	<button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
		<i class="fas fa-bars"></i>
	</button>
	<div class="ml-auto">
	</div>
	<!-- Navbar -->
	<ul class="navbar-nav ml-auto ml-md-0 float-right">
		<li class="nav-item dropdown no-arrow mx-1">
			<a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown"
			   aria-haspopup="true" aria-expanded="false">
				<i class="fas fa-bell fa-fw"></i>
				<span class="badge badge-danger">9+</span>
			</a>
			<div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
				<a class="dropdown-item" href="#">Action</a>
				<a class="dropdown-item" href="#">Another action</a>
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="#">Something else here</a>
			</div>
		</li>
		<li class="nav-item dropdown no-arrow mx-1">
			<a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown"
			   aria-haspopup="true" aria-expanded="false">
				<i class="fas fa-envelope fa-fw"></i>
				<span class="badge badge-danger">7</span>
			</a>
			<div class="dropdown-menu dropdown-menu-right" aria-labelledby="messagesDropdown">
				<a class="dropdown-item" href="#">Action</a>
				<a class="dropdown-item" href="#">Another action</a>
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="#">Something else here</a>
			</div>
		</li>
		<li class="nav-item dropdown no-arrow">
			<a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
			   aria-haspopup="true" aria-expanded="false">
				<?php if ($session->read('Auth.User.profile_image')){
					echo $this->Html->image("/files/Users/profile_image/".$session->read('Auth.User.profile_image'), [
						'class' => 'rounded-circle',
						'style' => 'width:20px'
					]);
					echo '<span> </span>'.$session->read('Auth.User.first_name')?>
					<?= $session->read('Auth.User.last_name');
				} else
					echo '<i class="fas fa-user-circle fa-fw"></i>'; ?>
			</a>
			<div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
				<a class="dropdown-item" href="/users/view/<?= $session->read('Auth.User.id') ?>">
					<i class="fas fa-user-cog"></i> Personal Settings</a>
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
					<i class="fas fa-sign-out-alt"></i> Logout</a>
			</div>
		</li>
	</ul>
</nav>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger" href="/users/logout">Logout</a>
            </div>
        </div>
    </div>
</div>