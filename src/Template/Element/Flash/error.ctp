<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="message alert alert-danger" role="alert" onclick="this.classList.add('hidden');">
    <i class="fas fa-exclamation-circle"></i>
    <?= $message ?>
</div>