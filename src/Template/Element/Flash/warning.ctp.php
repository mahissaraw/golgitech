<div class="row">
	<div class="col-xs-12">
		<div class="alert alert-warning" onclick="this.classList.add('hidden');">
			<i class="fas fa-exclamation-triangle"></i>
			<?= h($message) ?>
		</div>
	</div>
</div>