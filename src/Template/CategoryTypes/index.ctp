<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CategoryType[]|\Cake\Collection\CollectionInterface $categoryTypes
 */
?>
<div class="dropdown">
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right btn-sm" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><?= $this->Html->link(__('New Category Type'), ['action' => 'add'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add'], ['class' => "dropdown-item"]) ?></li>
    </ul>
</div>
<div class="categoryTypes index large-9 medium-8 columns content">
    <h3><?= __('Category Types') ?></h3>
    <div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($categoryTypes as $categoryType): ?>
            <tr>
                <td><?= $this->Number->format($categoryType->id) ?></td>
                <td><?= h($categoryType->name) ?></td>
                <td><?= h($categoryType->created) ?></td>
                <td><?= h($categoryType->modified) ?></td>
                <td class="actions">
                    <div class="btn-group btn-group-toggle">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $categoryType->id], ['class'=> 'btn btn-info btn-sm']) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $categoryType->id], ['class'=> 'btn btn-secondary btn-sm']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $categoryType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $categoryType->name), 'class'=> 'btn btn-danger btn-sm']) ?>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
