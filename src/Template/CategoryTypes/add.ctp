<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CategoryType $categoryType
 */
?>
<div class="dropdown">
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right btn-sm" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><?= $this->Html->link(__('List Category Types'), ['action' => 'index'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add'], ['class' => "dropdown-item"]) ?></li>
    </ul>
</div>
<div class="categoryTypes form large-9 medium-8 columns content">
    <?= $this->Form->create($categoryType) ?>
    <fieldset>
        <legend><?= __('Add Category Type') ?></legend>
        <div class="form-group">
        <?php
            echo $this->Form->control('name', ['class' => 'form-control']);
        ?>
        </div>
    </fieldset>
    <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
</div>
