<!--
image of the barcode (qr code)
batch ID
create date


-->
<style>
    .para-title{
        float:left;
        font-size: 0.6cm;
        display:inline;
        margin-top:0.05cm;
        margin-left: 0.1cm;
    }
    .para{
        float:left;
        font-size: 0.36cm;
        display:inline;
        margin-left:0.1cm;
        padding-top: 0.3cm;
    }
    .qri{
        float:left;
        margin-left: 0.1cm;
        margin-top: 0.2cm;
        width: auto;
        height:1.2cm;
    }
</style>

    <img class="qri" src="<?= ROOT.DS.'webroot'.DS.'img'.DS.'qr'.DS.$id.'.png'; ?>"/>
    <h1 class="para-title">
        <?= $id; ?>
    </h1>
    <p class="para">
        <?= $created_year; ?> [<?= $created_week; ?>]
    </p>





