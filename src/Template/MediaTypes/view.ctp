<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MediaType $mediaType
 */
?>
<div class='dropdown'>
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right btn-sm" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><?= $this->Html->link(__('Edit Media Type'), ['action' => 'edit', $mediaType->id], ['class'=>"dropdown-item"]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Media Type'), ['action' => 'delete', $mediaType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mediaType->id),'class'=>"dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('List Media Types'), ['action' => 'index'], ['class'=>"dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('New Media Type'), ['action' => 'add'], ['class'=>"dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('List Containers'), ['controller' => 'Containers', 'action' => 'index'], ['class'=>"dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('New Container'), ['controller' => 'Containers', 'action' => 'add'], ['class'=>"dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('List Medias'), ['controller' => 'Medias', 'action' => 'index'], ['class'=>"dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('New Media'), ['controller' => 'Medias', 'action' => 'add'], ['class'=>"dropdown-item"]) ?> </li>
    </ul>
</div>
<div class="mediaTypes view large-9 medium-8 columns content">
    <h3><?= h($mediaType->name) ?></h3>
    <table class="table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($mediaType->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($mediaType->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($mediaType->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($mediaType->modified) ?></td>
        </tr>
    </table>
    <div class="table">
        <h4><?= __('Related Containers') ?></h4>
        <?php if (!empty($mediaType->containers)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Item Capacity') ?></th>
                <th scope="col"><?= __('Media Capacity') ?></th>
                <th scope="col"><?= __('Media Type Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($mediaType->containers as $containers): ?>
            <tr>
                <td><?= h($containers->id) ?></td>
                <td><?= h($containers->name) ?></td>
                <td><?= h($containers->item_capacity) ?></td>
                <td><?= h($containers->media_capacity) ?></td>
                <td><?= h($containers->media_type_id) ?></td>
                <td><?= h($containers->created) ?></td>
                <td><?= h($containers->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Containers', 'action' => 'view', $containers->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Containers', 'action' => 'edit', $containers->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Containers', 'action' => 'delete', $containers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $containers->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Medias') ?></h4>
        <?php if (!empty($mediaType->medias)): ?>
        <table class="table">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Media Type Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($mediaType->medias as $medias): ?>
            <tr>
                <td><?= h($medias->id) ?></td>
                <td><?= h($medias->name) ?></td>
                <td><?= h($medias->media_type_id) ?></td>
                <td><?= h($medias->created) ?></td>
                <td><?= h($medias->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Medias', 'action' => 'view', $medias->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Medias', 'action' => 'edit', $medias->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Medias', 'action' => 'delete', $medias->id], ['confirm' => __('Are you sure you want to delete # {0}?', $medias->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
