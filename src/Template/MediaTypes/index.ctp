<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MediaType[]|\Cake\Collection\CollectionInterface $mediaTypes
 */
?>
<div class="dropdown">
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><?= $this->Html->link(__('New Media Type'), ['action' => 'add'],['class'=>"dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Containers'), ['controller' => 'Containers', 'action' => 'index'],['class'=>"dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New Container'), ['controller' => 'Containers', 'action' => 'add'],['class'=>"dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Medias'), ['controller' => 'Medias', 'action' => 'index'],['class'=>"dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New Media'), ['controller' => 'Medias', 'action' => 'add'],['class'=>"dropdown-item"]) ?></li>
    </ul>
</div>
<div class="table-responsive">
    <h3><?= __('Media Types') ?></h3>
    <div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($mediaTypes as $mediaType): ?>
            <tr>
                <td><?= $this->Number->format($mediaType->id) ?></td>
                <td><?= h($mediaType->name) ?></td>
                <td><?= h($mediaType->created) ?></td>
                <td><?= h($mediaType->modified) ?></td>
                <td class="actions">
                    <div class="btn-group btn-group-toggle">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $mediaType->id], ['class'=> 'btn btn-info btn-sm']) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $mediaType->id], ['class'=> 'btn btn-secondary btn-sm']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $mediaType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mediaType->id), 'class'=> 'btn btn-danger btn-sm']) ?>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
