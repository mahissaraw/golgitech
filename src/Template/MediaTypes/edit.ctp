<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MediaType $mediaType
 */
?>
<div class="dropdown">
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $mediaType->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $mediaType->id),'class'=>"dropdown-item"]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Media Types'), ['action' => 'index'], ['class'=>"dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Containers'), ['controller' => 'Containers', 'action' => 'index'], ['class'=>"dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New Container'), ['controller' => 'Containers', 'action' => 'add'], ['class'=>"dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Medias'), ['controller' => 'Medias', 'action' => 'index'], ['class'=>"dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New Media'), ['controller' => 'Medias', 'action' => 'add'], ['class'=>"dropdown-item"]) ?></li>
    </ul>
</div>
<div class="mediaTypes form large-9 medium-8 columns content">
    <?= $this->Form->create($mediaType) ?>
    <fieldset>
        <legend><?= __('Edit Media Type') ?></legend>
        <div class="form-group">
         <?= $this->Form->control('name', ['class' => 'form-control']) ?>
        </div>

    </fieldset>
    <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
</div>
