<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="dropdown">
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right btn-sm" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id], ['class'=>"dropdown-item"]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['class'=>"dropdown-item", 'confirm' => __('Are you sure you want to delete # {0}?', $user->full_name)]) ?>
        </li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index'],['class'=>"dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add'],['class'=>"dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('List Media Stocks'), ['controller' => 'MediaStocks', 'action' => 'index'],['class'=>"dropdown-item"]) ?>
        </li>
        <li><?= $this->Html->link(__('New Media Stock'), ['controller' => 'MediaStocks', 'action' => 'add'],['class'=>"dropdown-item"]) ?>
        </li>
        <li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index'],['class'=>"dropdown-item"]) ?>
        </li>
        <li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add'],['class'=>"dropdown-item"]) ?>
        </li>
    </ul>
</div>
<div class="users view large-9 medium-8 columns content">
    <h2>
        <?php 
        if ($user->profile_image){
            $image = $user->profile_image;
        } else {
            $image = "user.png";
        }
        echo $this->Html->image("/files/Users/profile_image/".$image, [
        'class' => 'rounded-circle img-thumbnail',
        'width' => '100px'
        ]); ?>
        <?= h($user->full_name) ?>
    </h2>
    <table class="table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('First Name') ?></th>
            <td><?= h($user->first_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Name') ?></th>
            <td><?= h($user->last_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Employment Number') ?></th>
            <td><?= h($user->employment_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Type') ?></th>
            <td><?= h($user->user_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Active') ?></th>
            <td><?= $user->active ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= $this->Time->format(
                $user->modified,
                'yyyy-MM-dd HH:mm:ss'
                ) ?>
            </td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= $this->Time->format(
                $user->created,
                'yyyy-MM-dd HH:mm:ss'
                ) ?>
            </td>
        </tr>

    </table>
    <div class="related">
        <h4><?= __('Related Media Stocks') ?></h4>
        <?php if (!empty($user->media_stocks)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Media Id') ?></th>
                <th scope="col"><?= __('Container Id') ?></th>
                <th scope="col"><?= __('Batch Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Quantity') ?></th>
                <th scope="col"><?= __('Qr Code') ?></th>
                <th scope="col"><?= __('Created By') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->media_stocks as $mediaStocks): ?>
            <tr>
                <td><?= h($mediaStocks->id) ?></td>
                <td><?= h($mediaStocks->media_id) ?></td>
                <td><?= h($mediaStocks->container_id) ?></td>
                <td><?= h($mediaStocks->batch_id) ?></td>
                <td><?= h($mediaStocks->user_id) ?></td>
                <td><?= h($mediaStocks->status) ?></td>
                <td><?= h($mediaStocks->quantity) ?></td>
                <td><?= h($mediaStocks->qr_code) ?></td>
                <td><?= h($mediaStocks->created_by) ?></td>
                <td><?= h($mediaStocks->created) ?></td>
                <td><?= h($mediaStocks->modified) ?></td>
                <td class="actions">
                    <div class="btn-group btn-group-toggle">
                    <?= $this->Html->link(__('View'), ['controller' => 'MediaStocks', 'action' => 'view', $mediaStocks->id], ['class'=> 'btn btn-info btn-sm']) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'MediaStocks', 'action' => 'edit', $mediaStocks->id], ['class'=> 'btn btn-secondary btn-sm']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'MediaStocks', 'action' => 'delete', $mediaStocks->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mediaStocks->id), 'class'=> 'btn btn-danger btn-sm']) ?>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Orders') ?></h4>
        <?php if (!empty($user->orders)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Customer Id') ?></th>
                <th scope="col"><?= __('Order Date') ?></th>
                <th scope="col"><?= __('Required Date') ?></th>
                <th scope="col"><?= __('Shipped Date') ?></th>
                <th scope="col"><?= __('Order Status') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->orders as $orders): ?>
            <tr>
                <td><?= h($orders->id) ?></td>
                <td><?= h($orders->customer_id) ?></td>
                <td><?= h($orders->order_date) ?></td>
                <td><?= h($orders->required_date) ?></td>
                <td><?= h($orders->shipped_date) ?></td>
                <td><?= h($orders->order_status) ?></td>
                <td><?= h($orders->user_id) ?></td>
                <td><?= h($orders->created) ?></td>
                <td><?= h($orders->modified) ?></td>
                <td class="actions">
                    <div class="btn-group btn-group-toggle">
                    <?= $this->Html->link(__('View'), ['controller' => 'Orders', 'action' => 'view', $orders->id], ['class'=> 'btn btn-info btn-sm']) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Orders', 'action' => 'edit', $orders->id],  ['class'=> 'btn btn-secondary btn-sm']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Orders', 'action' => 'delete', $orders->id], ['confirm' => __('Are you sure you want to delete # {0}?', $orders->id), 'class'=> 'btn btn-danger btn-sm']) ?>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>