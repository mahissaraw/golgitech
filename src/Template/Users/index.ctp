<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<div class='dropdown'>
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right btn-sm" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li>
            <?= $this->Html->link(__('New User'), ['action' => 'add'],['class'=>"dropdown-item"]) ?></li>
        <li>
            <?= $this->Html->link(__('List Media Stocks'), ['controller' => 'MediaStocks', 'action' => 'index'],['class'=>"dropdown-item"]) ?>
        </li>
        <li>
            <?= $this->Html->link(__('New Media Stock'), ['controller' => 'MediaStocks', 'action' => 'add'],['class'=>"dropdown-item"]) ?>
        </li>
        <li>
            <?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index'],['class'=>"dropdown-item"]) ?>
        </li>
        <li>
            <?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add'],['class'=>"dropdown-item"]) ?>
        </li>
    </ul>
</div>
<h3><?= __('Users') ?></h3>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col"></th>
                <th scope="col"><?= $this->Paginator->sort('first_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('last_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('employment_number') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('active') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
            <tr>
                <td>
                    <?php 
                    if ($user->profile_image){
                        $image = $user->profile_image;
                    } else {
                        $image = "user.png";
                    }
                    echo $this->Html->image("/files/Users/profile_image/".$image, [
                    'alt' => $image,
                    'class' => 'rounded-circle',
                    'style' => 'width:25px'
                    ]); ?>
                </td>
                <td><?= h($user->first_name) ?></td>
                <td><?= h($user->last_name) ?></td>
                <td><?= h($user->email) ?></td>
                <td><?= h($user->employment_number) ?></td>
                <td><?= h($user->user_type) ?></td>
                <td><?= h($user->active ? 'Yes' : 'No') ?></td>
                <td class="actions">
                    <div class="btn-group btn-group-toggle">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $user->id], ['class'=> 'btn btn-info btn-sm']) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $user->id], ['class'=> 'btn btn-secondary btn-sm']) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->full_name), 'class'=> 'btn btn-danger btn-sm']) ?>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('first')) ?>
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
        <?= $this->Paginator->last(__('last') . ' >>') ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?>
    </p>
</div>