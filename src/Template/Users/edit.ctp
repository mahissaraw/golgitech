<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class='dropdown'>
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right btn-sm" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $user->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $user->full_name), 'class' => "dropdown-item"]
            )
        ?>
        </li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Media Stocks'), ['controller' => 'MediaStocks', 'action' => 'index'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New Media Stock'), ['controller' => 'MediaStocks', 'action' => 'add'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add'], ['class' => "dropdown-item"]) ?></li>
    </ul>
</div>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user, ['type' => 'file']) ?>
    <fieldset>
        <legend><?= __('Edit User') ?></legend>
        <div class="form-group">
            <?= $this->Form->control('first_name', ['class' => 'form-control']) ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('last_name', ['class' => 'form-control']) ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('email', ['class' => 'form-control']) ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('password', ['class' => 'form-control']) ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('employment_number', ['class' => 'form-control']) ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('user_type', ['options' => ['user' => 'User', 'operator' => 'Operator', 'supervisor' => 'Supervisor', 'manager' => 'Manager', 'admin' => 'Admin'],
                'class' => 'form-control']); ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('profile_image', ['type' => 'file']) ?>
        </div>
        <div class="form-check">
            <?= $this->Form->control('active', ['class' => 'form-check-input']) ?>
        </div>
    </fieldset>
    <?= $this->Form->button(__('Submit'),['class' => 'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
</div>