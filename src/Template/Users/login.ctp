<div class="mx-auto">
    <div class="text-center">
        <img style="width: 400px;"
             src="/img/golgi_tec_logo.png"/>
    </div>
    <div class="card card-login mx-auto mt-3">
        <div class="card-header">Login</div>
        <div class="card-body">
	    <?= $this->Form->create() ?>
        <div class="form-group">
		    <?= $this->Form->control('email', ['class' => 'form-control']) ?>
        </div>
        <div class="form-group">
		    <?= $this->Form->control('password', ['class' => 'form-control']) ?>
        </div>
	    <?= $this->Form->button(__('<i class="fas fa-sign-in-alt"></i> Login'),
		    ['escape' => false, 'class' => 'btn btn-primary float-right float-sm-left']
	    ) ?>
	    <?= $this->Form->end() ?>
        </div>
    </div>
</div>


