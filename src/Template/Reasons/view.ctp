<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Reason $reason
 */
?>
<div class="dropdown">
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right btn-sm" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><?= $this->Html->link(__('Edit Reason'), ['action' => 'edit', $reason->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Reason'), ['action' => 'delete', $reason->id], ['confirm' => __('Are you sure you want to delete # {0}?', $reason->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Reasons'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Reason'), ['action' => 'add']) ?> </li>
    </ul>
</div>
<div class="reasons view large-9 medium-8 columns content">
    <h3><?= h($reason->name) ?></h3>
    <table class="table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($reason->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($reason->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($reason->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Name') ?></h4>
        <?= $this->Text->autoParagraph(h($reason->name)); ?>
    </div>
</div>
