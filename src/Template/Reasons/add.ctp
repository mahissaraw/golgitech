<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Reason $reason
 */
?>
<div class="dropdown">
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right btn-sm" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><?= $this->Html->link(__('List Reasons'), ['action' => 'index'],['class' => "dropdown-item"]) ?></li>
    </ul>
</div>
<div class="reasons form large-9 medium-8 columns content">
    <?= $this->Form->create($reason) ?>
    <fieldset>
        <legend><?= __('Add Reason') ?></legend>
        <div class="form-group">
		    <?php
		    echo $this->Form->control('name', ['class' => 'form-control']);
		    ?>
        </div>
    </fieldset>
	<?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
</div>
