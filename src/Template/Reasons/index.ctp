<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Reason[]|\Cake\Collection\CollectionInterface $reasons
 */
?>
<div class="dropdown">
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right btn-sm" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><?= $this->Html->link(__('New Reason'), ['action' => 'add'],['class' => "dropdown-item"]) ?></li>
    </ul>
</div>
<div class="reasons index large-9 medium-8 columns content">
    <h3><?= __('Reasons') ?></h3>
    <div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($reasons as $reason): ?>
            <tr>
                <td><?= $this->Number->format($reason->id) ?></td>
                <td><?= h($reason->name) ?></td>
                <td><?= h($reason->created) ?></td>
                <td><?= h($reason->modified) ?></td>
                <td class="actions">
                    <div class="btn-group btn-group-toggle">
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $reason->id], ['class'=> 'btn btn-secondary btn-sm']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $reason->id], ['confirm' => __('Are you sure you want to delete # {0}?', $reason->name), 'class'=> 'btn btn-danger btn-sm']) ?>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?>
        </p>
    </div>
</div>