<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ProcessingProductStock $processingProductStock
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Processing Product Stocks'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Medias'), ['controller' => 'Medias', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Media'), ['controller' => 'Medias', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Containers'), ['controller' => 'Containers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Container'), ['controller' => 'Containers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Batches'), ['controller' => 'Batches', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Batch'), ['controller' => 'Batches', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Stages'), ['controller' => 'Stages', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Stage'), ['controller' => 'Stages', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sections'), ['controller' => 'Sections', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Section'), ['controller' => 'Sections', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Rejected Reasons'), ['controller' => 'RejectedReasons', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Rejected Reason'), ['controller' => 'RejectedReasons', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="processingProductStocks form large-9 medium-8 columns content">
    <?= $this->Form->create($processingProductStock) ?>
    <fieldset>
        <legend><?= __('Add Processing Product Stock') ?></legend>
        <?php
            echo $this->Form->control('media_id', ['options' => $medias]);
            echo $this->Form->control('container_id', ['options' => $containers]);
            echo $this->Form->control('product_id', ['options' => $products]);
            echo $this->Form->control('batch_id', ['options' => $batches]);
            echo $this->Form->control('stage_id', ['options' => $stages]);
            echo $this->Form->control('section_id', ['options' => $sections]);
            echo $this->Form->control('rejected_reason_id', ['options' => $rejectedReasons]);
            echo $this->Form->control('number_of_plants');
            echo $this->Form->control('assigned_user_id');
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('is_accept');
            echo $this->Form->control('is_transferred');
            echo $this->Form->control('is_completed');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
