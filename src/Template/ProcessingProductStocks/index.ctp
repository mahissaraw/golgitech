<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ProcessingProductStock[]|\Cake\Collection\CollectionInterface $processingProductStocks
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Processing Product Stock'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Medias'), ['controller' => 'Medias', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Media'), ['controller' => 'Medias', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Containers'), ['controller' => 'Containers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Container'), ['controller' => 'Containers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Batches'), ['controller' => 'Batches', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Batch'), ['controller' => 'Batches', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Stages'), ['controller' => 'Stages', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Stage'), ['controller' => 'Stages', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sections'), ['controller' => 'Sections', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Section'), ['controller' => 'Sections', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Rejected Reasons'), ['controller' => 'RejectedReasons', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Rejected Reason'), ['controller' => 'RejectedReasons', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="processingProductStocks index large-9 medium-8 columns content">
    <h3><?= __('Processing Product Stocks') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('media_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('container_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('product_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('batch_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('stage_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('section_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('rejected_reason_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('number_of_plants') ?></th>
                <th scope="col"><?= $this->Paginator->sort('assigned_user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_accept') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_transferred') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_completed') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($processingProductStocks as $processingProductStock): ?>
            <tr>
                <td><?= $this->Number->format($processingProductStock->id) ?></td>
                <td><?= $processingProductStock->has('media') ? $this->Html->link($processingProductStock->media->name, ['controller' => 'Medias', 'action' => 'view', $processingProductStock->media->id]) : '' ?></td>
                <td><?= $processingProductStock->has('container') ? $this->Html->link($processingProductStock->container->name, ['controller' => 'Containers', 'action' => 'view', $processingProductStock->container->id]) : '' ?></td>
                <td><?= $processingProductStock->has('product') ? $this->Html->link($processingProductStock->product->name, ['controller' => 'Products', 'action' => 'view', $processingProductStock->product->id]) : '' ?></td>
                <td><?= $processingProductStock->has('batch') ? $this->Html->link($processingProductStock->batch->id, ['controller' => 'Batches', 'action' => 'view', $processingProductStock->batch->id]) : '' ?></td>
                <td><?= $processingProductStock->has('stage') ? $this->Html->link($processingProductStock->stage->name, ['controller' => 'Stages', 'action' => 'view', $processingProductStock->stage->id]) : '' ?></td>
                <td><?= $processingProductStock->has('section') ? $this->Html->link($processingProductStock->section->name, ['controller' => 'Sections', 'action' => 'view', $processingProductStock->section->id]) : '' ?></td>
                <td><?= $processingProductStock->has('rejected_reason') ? $this->Html->link($processingProductStock->rejected_reason->id, ['controller' => 'RejectedReasons', 'action' => 'view', $processingProductStock->rejected_reason->id]) : '' ?></td>
                <td><?= $this->Number->format($processingProductStock->number_of_plants) ?></td>
                <td><?= $this->Number->format($processingProductStock->assigned_user_id) ?></td>
                <td><?= $processingProductStock->has('user') ? $this->Html->link($processingProductStock->user->full_name, ['controller' => 'Users', 'action' => 'view', $processingProductStock->user->id]) : '' ?></td>
                <td><?= h($processingProductStock->is_accept) ?></td>
                <td><?= h($processingProductStock->is_transferred) ?></td>
                <td><?= h($processingProductStock->is_completed) ?></td>
                <td><?= h($processingProductStock->created) ?></td>
                <td><?= h($processingProductStock->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $processingProductStock->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $processingProductStock->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $processingProductStock->id], ['confirm' => __('Are you sure you want to delete # {0}?', $processingProductStock->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
