<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ProcessingProductStock $processingProductStock
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Processing Product Stock'), ['action' => 'edit', $processingProductStock->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Processing Product Stock'), ['action' => 'delete', $processingProductStock->id], ['confirm' => __('Are you sure you want to delete # {0}?', $processingProductStock->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Processing Product Stocks'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Processing Product Stock'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Medias'), ['controller' => 'Medias', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Media'), ['controller' => 'Medias', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Containers'), ['controller' => 'Containers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Container'), ['controller' => 'Containers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Batches'), ['controller' => 'Batches', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Batch'), ['controller' => 'Batches', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Stages'), ['controller' => 'Stages', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Stage'), ['controller' => 'Stages', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sections'), ['controller' => 'Sections', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Section'), ['controller' => 'Sections', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Rejected Reasons'), ['controller' => 'RejectedReasons', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Rejected Reason'), ['controller' => 'RejectedReasons', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="processingProductStocks view large-9 medium-8 columns content">
    <h3><?= h($processingProductStock->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Media') ?></th>
            <td><?= $processingProductStock->has('media') ? $this->Html->link($processingProductStock->media->name, ['controller' => 'Medias', 'action' => 'view', $processingProductStock->media->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Container') ?></th>
            <td><?= $processingProductStock->has('container') ? $this->Html->link($processingProductStock->container->name, ['controller' => 'Containers', 'action' => 'view', $processingProductStock->container->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Product') ?></th>
            <td><?= $processingProductStock->has('product') ? $this->Html->link($processingProductStock->product->name, ['controller' => 'Products', 'action' => 'view', $processingProductStock->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Batch') ?></th>
            <td><?= $processingProductStock->has('batch') ? $this->Html->link($processingProductStock->batch->id, ['controller' => 'Batches', 'action' => 'view', $processingProductStock->batch->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Stage') ?></th>
            <td><?= $processingProductStock->has('stage') ? $this->Html->link($processingProductStock->stage->name, ['controller' => 'Stages', 'action' => 'view', $processingProductStock->stage->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Section') ?></th>
            <td><?= $processingProductStock->has('section') ? $this->Html->link($processingProductStock->section->name, ['controller' => 'Sections', 'action' => 'view', $processingProductStock->section->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rejected Reason') ?></th>
            <td><?= $processingProductStock->has('rejected_reason') ? $this->Html->link($processingProductStock->rejected_reason->id, ['controller' => 'RejectedReasons', 'action' => 'view', $processingProductStock->rejected_reason->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $processingProductStock->has('user') ? $this->Html->link($processingProductStock->user->full_name, ['controller' => 'Users', 'action' => 'view', $processingProductStock->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($processingProductStock->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Number Of Plants') ?></th>
            <td><?= $this->Number->format($processingProductStock->number_of_plants) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Assigned User Id') ?></th>
            <td><?= $this->Number->format($processingProductStock->assigned_user_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($processingProductStock->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($processingProductStock->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Accept') ?></th>
            <td><?= $processingProductStock->is_accept ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Transferred') ?></th>
            <td><?= $processingProductStock->is_transferred ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Completed') ?></th>
            <td><?= $processingProductStock->is_completed ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
