<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Stage $stage
 */
?>
<div class='dropdown'>
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right btn-sm" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $stage->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $stage->name),'class'=>"dropdown-item"]
            )
        ?>
        </li>
        <li><?= $this->Html->link(__('List Stages'), ['action' => 'index'],['class'=>"dropdown-item"]) ?></li>
    </ul>
</div>
<div class="stages form large-9 medium-8 columns content">
    <?= $this->Form->create($stage) ?>
    <fieldset>
        <legend><?= __('Edit Stage') ?></legend>
        <div class="form-group">
            <?php
            echo $this->Form->control('name', ['class' => 'form-control']);
        ?>
        </div>
    </fieldset>
    <?= $this->Form->button(__('Submit'),['class' => 'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
</div>