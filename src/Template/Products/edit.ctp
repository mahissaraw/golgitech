<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Product $product
 */
?>
<div class='dropdown'>
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right btn-sm" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $product->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $product->id), 'class'=>"dropdown-item"]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Products'), ['action' => 'index'], ['class'=>"dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index'], ['class'=>"dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add'], ['class'=>"dropdown-item"]) ?></li>
    </ul>
</div>
<div class="products form large-9 medium-8 columns content">
    <?= $this->Form->create($product) ?>
    <fieldset>
        <legend><?= __('Edit Product') ?></legend>
        <div class="form-group">
            <?= $this->Form->control('category_id', ['options' => $categories, 'class' => 'form-control']) ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('name', ['class' => 'form-control'])?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('code', ['class' => 'form-control'])?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('price', ['class' => 'form-control'])?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('bar_code', ['class' => 'form-control'])?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('description', ['class' => 'form-control'])?>
        </div>
    </fieldset>
    <?= $this->Form->button(__('Submit'),['class' => 'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
</div>
