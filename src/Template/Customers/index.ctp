<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Customer[]|\Cake\Collection\CollectionInterface $customers
 */
?>
<div class="col-sm-12">
    <div class="btn-group">
        <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right btn-sm" data-toggle="dropdown">
            Actions <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu">
        <li><?= $this->Html->link(__('New Customer'), ['action' => 'add'], ['class'=>"dropdown-item"]) ?></li>
    </ul>
    </div>
</div>

<div class="customers index large-9 medium-8 columns content">
    <h3><?= __('Customers') ?></h3>
    <div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('first_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('last_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('phone') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('street') ?></th>
                <th scope="col"><?= $this->Paginator->sort('city') ?></th>
                <th scope="col"><?= $this->Paginator->sort('state') ?></th>
                <th scope="col"><?= $this->Paginator->sort('country') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($customers as $customer): ?>
            <tr>
                <td><?= $this->Number->format($customer->id) ?></td>
                <td><?= h($customer->first_name) ?></td>
                <td><?= h($customer->last_name) ?></td>
                <td><?= $this->Number->format($customer->phone) ?></td>
                <td><?= h($customer->email) ?></td>
                <td><?= h($customer->street) ?></td>
                <td><?= h($customer->city) ?></td>
                <td><?= h($customer->state) ?></td>
                <td><?= h($customer->country) ?></td>
                <td><?= h($customer->created) ?></td>
                <td><?= h($customer->modified) ?></td>
                <td class="actions">
                    <div class="btn-group btn-group-toggle">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $customer->id], ['class'=> 'btn btn-info btn-sm']) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $customer->id], ['class'=> 'btn btn-secondary btn-sm']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $customer->id],['class'=> 'btn btn-danger btn-sm'], ['confirm' => __('Are you sure you want to delete # {0}?', $customer->id)]) ?>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

