<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Customer $customer
 */
?>
    <div class="btn-group">
        <button type="button" class="btn btn-warning btn-raised dropdown-toggle" data-toggle="dropdown">
            Actions <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $customer->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $customer->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Customers'), ['action' => 'index']) ?></li>
    </ul>
    </div>
<div class="customers form large-9 medium-8 columns content">
    <?= $this->Form->create($customer) ?>
    <fieldset>
        <legend><?= __('Edit Customer') ?></legend>
        <?php
            echo $this->Form->control('first_name', ['class' => 'form-control']);
            echo $this->Form->control('last_name', ['class' => 'form-control']);
            echo $this->Form->control('phone', ['class' => 'form-control']);
            echo $this->Form->control('email', ['class' => 'form-control']);
            echo $this->Form->control('street', ['class' => 'form-control']);
            echo $this->Form->control('city', ['class' => 'form-control']);
            echo $this->Form->control('state', ['class' => 'form-control']);
            echo $this->Form->control('country', ['class' => 'form-control']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'),
     ['class' => 'btn btn-primary btn-raised']
    ) ?>
    <?= $this->Form->end() ?>
</div>
