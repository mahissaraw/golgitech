<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Customer $customer
 */
?>
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group">
            <button type="button" class="btn btn-warning btn-raised dropdown-toggle btn-sm" data-toggle="dropdown">
                Actions <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
                <li><?= $this->Html->link(__('List Customers'), ['action' => 'index'], ['class'=>"dropdown-item"]) ?></li>
            </ul>
        </div>
    </div>
</div>
<div class="customers form large-9 medium-8 columns content" class="form-group">
    <?= $this->Form->create($customer) ?>
    <fieldset>
        <legend><?= __('Add Customer') ?></legend>
        <?php
            echo $this->Form->control('first_name', ['class' => 'form-control']);
            echo $this->Form->control('last_name', ['class' => 'form-control']);
            echo $this->Form->control('phone', ['class' => 'form-control']);
            echo $this->Form->control('email', ['class' => 'form-control']);
            echo $this->Form->control('street', ['class' => 'form-control']);
            echo $this->Form->control('city', ['class' => 'form-control']);
            echo $this->Form->control('state', ['class' => 'form-control']);
            echo $this->Form->control('country', ['class' => 'form-control']);
        ?>
    </fieldset>
    <div class="form-group">
        <?= $this->Form->button(__('Submit'),
            ['class' => 'btn btn-primary btn-raised']
        )?>
    </div>
    <?= $this->Form->end() ?>
</div>