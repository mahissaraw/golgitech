<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Media $media
 */
?>
<div class="dropdown">
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right btn-sm" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><?= $this->Html->link(__('Edit Media'), ['action' => 'edit', $media->id], ['class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Media'), ['action' => 'delete', $media->id], ['confirm' => __('Are you sure you want to delete # {0}?', $media->id), 'class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('List Medias'), ['action' => 'index'], ['class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('New Media'), ['action' => 'add'], ['class' => "dropdown-item"]) ?> </li>
    </ul>
</div>
<div class="medias view large-9 medium-8 columns content">
    <h3><?= h($media->name) ?></h3>
    <table class="table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($media->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Media Type Id') ?></th>
            <td><?= $media->has('media_type') ? $media->media_type->name : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($media->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($media->modified) ?></td>
        </tr>
    </table>
</div>
