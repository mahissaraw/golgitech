<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Media $media
 */
?>
<div class='dropdown'>
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right btn-sm" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $media->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $media->name), 'class' => "dropdown-item"]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Medias'), ['action' => 'index'], ['class' => "dropdown-item"]) ?></li>
    </ul>
</div>
<div class="medias form large-9 medium-8 columns content">
    <?= $this->Form->create($media) ?>
    <fieldset>
        <legend><?= __('Edit Media') ?></legend>
        <div class="form-group">
		    <?= $this->Form->control('name', ['class' => 'form-control']); ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('media_type_id', ['options' => $mediatypes,'class' => 'form-control']); ?>
        </div>
    </fieldset>
    <?= $this->Form->button(__('Submit'),['class' => 'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
</div>
