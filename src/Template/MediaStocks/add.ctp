<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MediaStock $mediaStock
 */
?>
<div class="dropdown">
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right btn-sm" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><?= $this->Html->link(__('List Media Stocks'), ['action' => 'index'], ['class'=>"dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Medias'), ['controller' => 'Medias', 'action' => 'index'], ['class'=>"dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New Media'), ['controller' => 'Medias', 'action' => 'add'], ['class'=>"dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Containers'), ['controller' => 'Containers', 'action' => 'index'], ['class'=>"dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New Container'), ['controller' => 'Containers', 'action' => 'add'], ['class'=>"dropdown-item"]) ?></li>
    </ul>
</div>
<div class="mediaStocks form large-9 medium-8 columns content">
    <?= $this->Form->create($mediaStock) ?>
    <fieldset>
        <legend><?= __('Add Media Stock') ?></legend>
        <div class="form-group">
            <?= $this->Form->control('media_id', ['options' => $medias, 'class' => 'form-control']); ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('container_id', ['options' => $containers, 'class' => 'form-control']);?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('assigned_user_id', ['options' => $users, 'class' => 'form-control']); ?>
        </div>

        <div class="form-group">
            <?= $this->Form->control('container_quantity', ['class' => 'form-control']); ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('autoclave', ['class' => 'form-control']);?>
        </div>
    </fieldset>
    <?= $this->Form->button(__('Submit'),['class' => 'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
</div>