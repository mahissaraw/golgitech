<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MediaStock[]|\Cake\Collection\CollectionInterface $mediaStocks
 */
?>
<div class="dropdown">
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right btn-sm" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><?= $this->Html->link(__('New Media Stock'), ['action' => 'add'], ['class'=>"dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Medias'), ['controller' => 'Medias', 'action' => 'index'], ['class'=>"dropdown-item"]) ?>
        </li>
        <li><?= $this->Html->link(__('New Media'), ['controller' => 'Medias', 'action' => 'add'], ['class'=>"dropdown-item"]) ?>
        </li>
        <li><?= $this->Html->link(__('List Containers'), ['controller' => 'Containers', 'action' => 'index'], ['class'=>"dropdown-item"]) ?>
        </li>
        <li><?= $this->Html->link(__('New Container'), ['controller' => 'Containers', 'action' => 'add'], ['class'=>"dropdown-item"]) ?>
        </li>
    </ul>
</div>
<div class="mediaStocks index large-9 medium-8 columns content">
    <h3><?= __('Media Stocks') ?></h3>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('media_id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('container_id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('batch_id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('assigned_user_id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('container_quantity') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('autoclave') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('created_user_id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($mediaStocks as $mediaStock): ?>
                <tr>
                    <td><?= $this->Number->format($mediaStock->id) ?></td>
                    <td><?= $mediaStock->has('media') ? $this->Html->link($mediaStock->media->name, ['controller' => 'Medias', 'action' => 'view', $mediaStock->media->id]) : '' ?>
                    </td>
                    <td><?= $mediaStock->has('container') ? $this->Html->link($mediaStock->container->name, ['controller' => 'Containers', 'action' => 'view', $mediaStock->container->id]) : '' ?>
                    </td>
                    <td><?= $mediaStock->has('batch') ? $this->Html->link($mediaStock->batch->id, ['controller' => 'Batches', 'action' => 'view', $mediaStock->batch->id]) : '' ?>
                    </td>
                    <td><?= $mediaStock->has('user') ? $mediaStock->user->first_name : '' ?></td>
                    <td><?= $this->Number->format($mediaStock->container_quantity) ?></td>
                    <td><?= $this->Number->format($mediaStock->autoclave) ?></td>
                    <td><?= $mediaStock->has('CreatedUsers') ? $mediaStock->CreatedUsers->first_name : '' ?></td>
                    <td><?= h($mediaStock->created) ?></td>
                    <td class="actions">
                        <div class="btn-group btn-group-toggle">
                            <?= $this->Html->link(__('View'), ['action' => 'view', $mediaStock->id], ['class'=> 'btn btn-info btn-sm']) ?>
                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $mediaStock->id], ['class'=> 'btn btn-secondary btn-sm']) ?>
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $mediaStock->id], ['class'=> 'btn btn-danger btn-sm'], ['confirm' => __('Are you sure you want to delete # {0}?', $mediaStock->id)]) ?>
                        </div>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?>
        </p>
    </div>
</div>