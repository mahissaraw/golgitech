<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MediaStock $mediaStock
 */
?>
<div class="dropdown">
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right btn-sm" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><?= $this->Html->link(__('Edit Media Stock'), ['action' => 'edit', $mediaStock->id], ['class'=>"dropdown-item"]) ?>
        </li>
        <li><?= $this->Form->postLink(__('Delete Media Stock'), ['action' => 'delete', $mediaStock->id], ['confirm' => __('Are you sure you want to delete # {0}?'), 'class' => "dropdown-item"]) ?>
        </li>
        <li><?= $this->Html->link(__('List Media Stocks'), ['action' => 'index'], ['class'=>"dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('New Media Stock'), ['action' => 'add'], ['class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('List Medias'), ['controller' => 'Medias', 'action' => 'index'], ['class'=>"dropdown-item"]) ?>
        </li>
        <li><?= $this->Html->link(__('New Media'), ['controller' => 'Medias', 'action' => 'add'], ['class'=>"dropdown-item"]) ?>
        </li>
        <li><?= $this->Html->link(__('List Containers'), ['controller' => 'Containers', 'action' => 'index'], ['class'=>"dropdown-item"]) ?>
        </li>
        <li><?= $this->Html->link(__('New Container'), ['controller' => 'Containers', 'action' => 'add'], ['class'=>"dropdown-item"]) ?>
        </li>
    </ul>
</div>
<div class="mediaStocks view large-9 medium-8 columns content">
    <h3><?= h($mediaStock->id) ?></h3>
    <table class="table">
        <tr>
            <th scope="row"><?= __('Media') ?></th>
            <td><?= $mediaStock->has('media') ? $this->Html->link($mediaStock->media->name, ['controller' => 'Medias', 'action' => 'view', $mediaStock->media->id]) : '' ?>
            </td>
        </tr>
        <tr>
            <th scope="row"><?= __('Container') ?></th>
            <td><?= $mediaStock->has('container') ? $this->Html->link($mediaStock->container->name, ['controller' => 'Containers', 'action' => 'view', $mediaStock->container->id]) : '' ?>
            </td>
        </tr>
        <tr>
            <th scope="row"><?= __('Batch') ?></th>
            <td><?= $mediaStock->has('batch') ? $this->Html->link($mediaStock->batch->id, ['controller' => 'Batches', 'action' => 'view', $mediaStock->batch->id]) : '' ?>
            </td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($mediaStock->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Assigned User') ?></th>
            <td><?= $mediaStock->has('user') ? $mediaStock->user->full_name : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Container Quantity') ?></th>
            <td><?= $this->Number->format($mediaStock->container_quantity) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Autoclave') ?></th>
            <td><?= $this->Number->format($mediaStock->autoclave) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created User') ?></th>
            <td><?= $mediaStock->has('CreatedUsers') ? $mediaStock->CreatedUsers->full_name : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Completed') ?></th>
            <td><?= $mediaStock->completed ? 'Yes' : 'No' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($mediaStock->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($mediaStock->modified) ?></td>
        </tr>
    </table>
</div>
    <!-- Print label -->
<div class="row">
    <div class="col-12">
        <button class="btn btn-outline-primary print"
                data-product-id="<?= $mediaStock->id ?>"
                data-loading-text="<i class='glyphicon glyphicon-cog gly-spin'></i> Printing...">
            Print label
        </button>
    </div>
</div>
<script>
    $(document).ready(function($){
        $('button.print').on('click', function(event) {
            event.preventDefault();
            if (confirm('Are you sure you want reprint label?')){
                // Start animation to show progress
                $(this).toggleClass("btn-disabled");
                $(this).prop('disabled', true);
                $(this).attr('disabled', 'disabled');
                $(this).button('loading');
                $.get('/media-stocks/label/' + $(this).attr('data-product-id'), function (response) {
                    response = JSON.parse(response);
                    // Get the button that signaled
                    let btn = $("button.print[data-product-id='" + response.id + "']");
                    $(btn).button('reset');
                    $(btn).toggleClass("btn-disabled");
                    $(btn).prop('disabled', false);
                    $(btn).removeAttr('disabled');
                    $(btn).parent().append("<div class=\"message alert "+ response.alert+"\" role=\"alert\" onclick=\"this.classList.add('hidden')\">" + response.message + "</div>");
                });
            }
        });

    });
</script>