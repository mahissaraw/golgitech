<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MediaStock $mediaStock
 */
?>
<div class="dropdown">
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right btn-sm" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $mediaStock->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $mediaStock->id), 'class' => "dropdown-item"]
            )
        ?>
        </li>
        <li><?= $this->Html->link(__('List Media Stocks'), ['action' => 'index'], ['class'=>"dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Medias'), ['controller' => 'Medias', 'action' => 'index'], ['class'=>"dropdown-item"]) ?>
        </li>
        <li><?= $this->Html->link(__('New Media'), ['controller' => 'Medias', 'action' => 'add'], ['class'=>"dropdown-item"]) ?>
        </li>
        <li><?= $this->Html->link(__('List Containers'), ['controller' => 'Containers', 'action' => 'index'], ['class'=>"dropdown-item"]) ?>
        </li>
        <li><?= $this->Html->link(__('New Container'), ['controller' => 'Containers', 'action' => 'add'], ['class'=>"dropdown-item"]) ?>
        </li>
        <li><?= $this->Html->link(__('List Batches'), ['controller' => 'Batches', 'action' => 'index'], ['class'=>"dropdown-item"]) ?>
        </li>
        <li><?= $this->Html->link(__('New Batch'), ['controller' => 'Batches', 'action' => 'add'], ['class'=>"dropdown-item"]) ?>
        </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index'], ['class'=>"dropdown-item"]) ?>
        </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add'], ['class'=>"dropdown-item"]) ?>
        </li>
    </ul>
</div>
<div class="mediaStocks form large-9 medium-8 columns content">
    <?= $this->Form->create($mediaStock) ?>
    <fieldset>
        <legend><?= __('Edit Media Stock') ?></legend>
        <div class="form-group">
            <?= $this->Form->control('media_id', ['options' => $medias, 'class' => 'form-control']) ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('container_id', ['options' => $containers, 'class' => 'form-control']) ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('batch_id', ['disabled' => 'disabled', 'class' => 'form-control']) ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('assigned_user_id', ['options' => $users, 'class' => 'form-control']) ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('container_quantity', ['class' => 'form-control']) ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('autoclave', ['class' => 'form-control']) ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('created_user_id', ['options' => $users, 'class' => 'form-control', 'disabled' => 'disabled']) ?>
        </div>
    </fieldset>
    <?= $this->Form->button(__('Submit'),['class' => 'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
</div>