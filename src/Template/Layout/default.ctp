<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

echo $this->Html->script('bootstrap.bundle.min.js', ['block' => true]);
echo $this->Html->script('jquery.easing.min.js', ['block' => true]);
echo $this->Html->script('Chart.min.js', ['block' => true]);
echo $this->Html->script('jquery.dataTables.js', ['block' => true]);
echo $this->Html->script('dataTables.bootstrap4.js', ['block' => true]);
echo $this->Html->script('sb-admin.min.js', ['block' => true]);
echo $this->Html->script('sb-admin.js', ['block' => true]);
echo $this->Html->script('datatables-demo.js', ['block' => true]);
echo $this->Html->script('chart-area-demo.js', ['block' => true]);
echo $this->Html->script('bootstrap.min.js', ['block' => true]);

$cakeDescription = 'Golgi Tech Management System';

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
              integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title> <?= $this->fetch('title') ?> </title>

        <?= $this->Html->css('sb-admin.css'); ?>
        <?= $this->Html->css('dataTables.bootstrap4.css'); ?>
        <?= $this->Html->css('main.css'); ?>
    </head>
    <body id="page-top">
        <!-- Top Bar -->
        <?= $this->element('top_bar'); ?>
        <div id="wrapper">
            <!-- Menu -->
            <?= $this->element('menu'); ?>
            <div id="content-wrapper">
                <div class="container-fluid">
                <div class="col-xs-12">
                    <?= $this->Flash->render(); ?>
                </div>
                <div class="col-xs-12">
                    <?= $this->fetch('content') ?>
                </div>
            </div>
        </div>
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>
	    <?= $this->fetch('scriptBottom'); ?>
    </body>

</html>