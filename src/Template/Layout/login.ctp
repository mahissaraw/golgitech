<?= $this->Html->script('materialize.min.js', ['block' => 'scriptBottom']) ?>
<!DOCTYPE html>

<head>
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <meta name="theme-color" content="#014228" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<?= $this->Html->css('sb-admin.css'); ?>
	<?= $this->Html->css('main.css'); ?>
</head>

<body class="bg-dark">
    <div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <?= $this->Flash->render(); ?>
                </div>
                <div class="col-12">
                    <?= $this->fetch('content') ?>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
</html>