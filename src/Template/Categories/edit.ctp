<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Category $category
 */
?>
<div class="dropdown">
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right btn-sm" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $category->name],
                ['confirm' => __('Are you sure you want to delete # {0}?', $category->id), 'class' => "dropdown-item"]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Categories'), ['action' => 'index'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Category Types'), ['controller' => 'CategoryTypes', 'action' => 'index'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New Category Type'), ['controller' => 'CategoryTypes', 'action' => 'add'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index'], ['class' => "dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add'], ['class' => "dropdown-item"]) ?></li>
    </ul>
</div>
<div class="categories form large-9 medium-8 columns content">
    <?= $this->Form->create($category) ?>
    <fieldset>
        <legend><?= __('Add Category') ?></legend>
        <div class="form-group">
			<?= $this->Form->control('category_type_id', ['options' => $categoryTypes, 'class' => 'form-control']); ?>
        </div>
        <div class="form-group">
			<?= $this->Form->control('name', ['class' => 'form-control']); ?>
    </fieldset>
	<?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
</div>
