<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Category $category
 */
?>
<div class="dropdown">
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right btn-sm" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><?= $this->Html->link(__('Edit Category'), ['action' => 'edit', $category->id], ['class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Category'), ['action' => 'delete', $category->name], ['confirm' => __('Are you sure you want to delete # {0}?', $category->id), 'class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('List Categories'), ['action' => 'index'], ['class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('New Category'), ['action' => 'add'], ['class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('List Category Types'), ['controller' => 'CategoryTypes', 'action' => 'index'], ['class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('New Category Type'), ['controller' => 'CategoryTypes', 'action' => 'add'], ['class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index'], ['class' => "dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add'], ['class' => "dropdown-item"]) ?> </li>
    </ul>
</div>
<div class="categories view large-9 medium-8 columns content">
    <h3><?= h($category->name) ?></h3>
    <table class="table">
        <tr>
            <th scope="row"><?= __('Category Type') ?></th>
            <td><?= $category->has('category_type') ? $this->Html->link($category->category_type->name, ['controller' => 'CategoryTypes', 'action' => 'view', $category->category_type->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($category->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($category->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($category->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($category->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Products') ?></h4>
        <?php if (!empty($category->products)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Category Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Code') ?></th>
                <th scope="col"><?= __('Price') ?></th>
                <th scope="col"><?= __('Bar Code') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($category->products as $products): ?>
            <tr>
                <td><?= h($products->id) ?></td>
                <td><?= h($products->category_id) ?></td>
                <td><?= h($products->name) ?></td>
                <td><?= h($products->code) ?></td>
                <td><?= h($products->price) ?></td>
                <td><?= h($products->bar_code) ?></td>
                <td><?= h($products->description) ?></td>
                <td><?= h($products->created) ?></td>
                <td><?= h($products->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Products', 'action' => 'view', $products->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Products', 'action' => 'edit', $products->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Products', 'action' => 'delete', $products->id], ['confirm' => __('Are you sure you want to delete # {0}?', $products->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
