<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Container[]|\Cake\Collection\CollectionInterface $containers
 */
?>
<div class='dropdown'>
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right btn-sm" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><?= $this->Html->link(__('New Container'), ['action' => 'add'],['class'=>"dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Media Stocks'), ['controller' => 'MediaStocks', 'action' => 'index'],['class'=>"dropdown-item"]) ?>
        </li>
        <li><?= $this->Html->link(__('New Media Stock'), ['controller' => 'MediaStocks', 'action' => 'add'],['class'=>"dropdown-item"]) ?>
        </li>
    </ul>
</div>
<h3><?= __('Containers') ?></h3>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('item_capacity') ?></th>
                <th scope="col"><?= $this->Paginator->sort('media_capacity') ?></th>
                <th scope="col"><?= $this->Paginator->sort('media_type_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($containers as $container): ?>
            <tr>
                <td><?= $this->Number->format($container->id) ?></td>
                <td><?= h($container->name) ?></td>
                <td><?= $this->Number->format($container->item_capacity) ?></td>
                <td><?= $this->Number->format($container->media_capacity) ?></td>
                <td><?= $container->has('media_type')? $container->media_type->name : '' ?></td>
                <td><?= h($container->created) ?></td>
                <td><?= h($container->modified) ?></td>
                <td class="actions">
                    <div class="btn-group btn-group-toggle">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $container->id], ['class'=> 'btn btn-info btn-sm']) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $container->id], ['class'=> 'btn btn-secondary btn-sm']) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $container->id], ['confirm' => __('Are you sure you want to delete # {0}?', $container->id), 'class'=> 'btn btn-danger btn-sm']) ?>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('first')) ?>
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
        <?= $this->Paginator->last(__('last') . ' >>') ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?>
    </p>
</div>