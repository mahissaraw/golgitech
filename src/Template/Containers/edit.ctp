<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Container $container
 */
?>
<div class="dropdown">
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right btn-sm" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $container->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $container->name), 'class'=>"dropdown-item"]
            )
            ?></li>
        <li><?= $this->Html->link(__('List Containers'), ['action' => 'index'], ['class'=>"dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('List Media Stocks'), ['controller' => 'MediaStocks', 'action' => 'index'], ['class'=>"dropdown-item"]) ?></li>
        <li><?= $this->Html->link(__('New Media Stock'), ['controller' => 'MediaStocks', 'action' => 'add'], ['class'=>"dropdown-item"]) ?></li>
    </ul>
</div>
<div class="containers form large-9 medium-8 columns content">
    <?= $this->Form->create($container) ?>
    <fieldset>
        <legend><?= __('Edit Container') ?></legend>
        <div class="form-group">
            <?= $this->Form->control('name', ['class' => 'form-control']) ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('item_capacity', ['class' => 'form-control']) ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('media_capacity', ['class' => 'form-control']) ?>
        </div>
        <div class="form-group">
            <?= $this->Form->control('media_type_id', ['options' => $mediatypes, 'class' => 'form-control']) ?>
        </div>
    </fieldset>
    <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
</div>
