<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Container $container
 */
?>
<div class="dropdown">
    <button type="button" class="btn btn-warning btn-raised dropdown-toggle float-right btn-sm" data-toggle="dropdown">
        Actions <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><?= $this->Html->link(__('Edit Container'), ['action' => 'edit', $container->id], ['class'=>"dropdown-item"]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Container'), ['action' => 'delete', $container->id], ['confirm' => __('Are you sure you want to delete # {0}?', $container->id), 'class'=>"dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('List Containers'), ['action' => 'index'], ['class'=>"dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('New Container'), ['action' => 'add'], ['class'=>"dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('List Media Stocks'), ['controller' => 'MediaStocks', 'action' => 'index'], ['class'=>"dropdown-item"]) ?> </li>
        <li><?= $this->Html->link(__('New Media Stock'), ['controller' => 'MediaStocks', 'action' => 'add'], ['class'=>"dropdown-item"]) ?> </li>
    </ul>
</div>
<div class="containers view large-9 medium-8 columns content">
    <h3><?= h($container->name) ?></h3>
    <table class="table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($container->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($container->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Item Capacity') ?></th>
            <td><?= $this->Number->format($container->item_capacity) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Media Capacity') ?></th>
            <td><?= $this->Number->format($container->media_capacity) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Media Type') ?></th>
            <td><?= $container->has('media_type') ? $container->media_type->name : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= $this->Time->format(
                    $container->created,
                    'yyyy-MM-dd HH:mm:ss'
                ) ?>
            </td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= $this->Time->format(
                    $container->modified,
                    'yyyy-MM-dd HH:mm:ss'
                ) ?>
            </td>
        </tr>
    </table><h4><?= __('Related Media Stocks') ?></h4>
    <div class="table-responsive">
        <?php if (!empty($container->media_stocks)): ?>
        <table class="table table-striped"">
            <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('media_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('container_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('batch_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('quantity') ?></th>
                <th scope="col"><?= $this->Paginator->sort('qr_code') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created_by') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($container->media_stocks as $mediaStocks): ?>
                <tr>
                    <td><?= h($mediaStocks->id) ?></td>
                    <td><?= h($mediaStocks->media_id) ?></td>
                    <td><?= h($mediaStocks->container_id) ?></td>
                    <td><?= h($mediaStocks->batch_id) ?></td>
                    <td><?= h($mediaStocks->user_id) ?></td>
                    <td><?= h($mediaStocks->status) ?></td>
                    <td><?= h($mediaStocks->quantity) ?></td>
                    <td><?= h($mediaStocks->qr_code) ?></td>
                    <td><?= h($mediaStocks->created_by) ?></td>
                    <td><?= h($mediaStocks->created) ?></td>
                    <td><?= h($mediaStocks->modified) ?></td>
                    <td class="actions">
                        <div class="btn-group btn-group-toggle">
                        <?= $this->Html->link(__('View'), ['controller' => 'MediaStocks', 'action' => 'view', $mediaStocks->id], ['class'=> 'btn btn-info btn-sm']) ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'MediaStocks', 'action' => 'edit', $mediaStocks->id], ['class'=> 'btn btn-secondary btn-sm']) ?>
                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'MediaStocks', 'action' => 'delete', $mediaStocks->id],['class'=> 'btn btn-danger btn-sm'], ['confirm' => __('Are you sure you want to delete # {0}?', $mediaStocks->id)]) ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <?php endif; ?>
    </div>


</div>
