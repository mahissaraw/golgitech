<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MediaStocks Model
 *
 * @property \App\Model\Table\MediasTable|\Cake\ORM\Association\BelongsTo $Medias
 * @property \App\Model\Table\ContainersTable|\Cake\ORM\Association\BelongsTo $Containers
 * @property \App\Model\Table\BatchesTable|\Cake\ORM\Association\BelongsTo $Batches
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $CreatedUsers
 *
 * @method \App\Model\Entity\MediaStock get($primaryKey, $options = [])
 * @method \App\Model\Entity\MediaStock newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MediaStock[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MediaStock|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MediaStock|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MediaStock patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MediaStock[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MediaStock findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MediaStocksTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('media_stocks');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Medias', [
            'foreignKey' => 'media_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Containers', [
            'foreignKey' => 'container_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Batches', [
            'foreignKey' => 'batch_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'assigned_user_id',
            'joinType' => 'INNER'
        ]);
        // $this->belongsTo('Users', [
        //     'foreignKey' => 'created_user_id',
        //     'joinType' => 'INNER'
        // ]);

        $this->belongsTo('CreatedUsers', [
            'className' => 'Users',
            'foreignKey' => 'created_user_id',
            'joinType' => 'INNER',
            'propertyName' => 'CreatedUsers'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->integer('container_quantity')
            ->requirePresence('container_quantity', 'create')
            ->allowEmptyString('container_quantity', false);

        $validator
            ->integer('autoclave')
            ->allowEmptyString('autoclave');

        $validator
            ->dateTime('completed')
            ->allowEmptyDateTime('completed');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['media_id'], 'Medias'));
        $rules->add($rules->existsIn(['container_id'], 'Containers'));
        $rules->add($rules->existsIn(['batch_id'], 'Batches'));
        $rules->add($rules->existsIn(['assigned_user_id'], 'Users'));
        $rules->add($rules->existsIn(['created_user_id'], 'Users'));

        return $rules;
    }
}