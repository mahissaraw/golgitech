<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Container Entity
 *
 * @property int $id
 * @property string $name
 * @property int $item_capacity
 * @property int $media_capacity
 * @property int $media_type_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\MediaStock[] $media_stocks
 */
class Container extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'item_capacity' => true,
        'media_capacity' => true,
        'media_type_id' => true,
        'created' => true,
        'modified' => true,
        'media_stocks' => true
    ];

    public function _getNameWithType(){
        $mediaTypes = TableRegistry::getTableLocator()->get('MediaTypes')
            ->findById($this->get('media_type_id'))
            ->first();
        return $this->get('name')." [".$mediaTypes->name."]";
    }
}
