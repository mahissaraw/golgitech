<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MediaStock Entity
 *
 * @property int $id
 * @property int $media_id
 * @property int $container_id
 * @property int $batch_id
 * @property int $assigned_user_id
 * @property int $container_quantity
 * @property int|null $autoclave
 * @property int $created_user_id
 * @property \Cake\I18n\FrozenTime|null $completed
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Media $media
 * @property \App\Model\Entity\Container $container
 * @property \App\Model\Entity\Batch $batch
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\User $Assign
 * @property \App\Model\Entity\User $Generate
 */
class MediaStock extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'media_id' => true,
        'container_id' => true,
        'batch_id' => true,
        'assigned_user_id' => true,
        'container_quantity' => true,
        'autoclave' => true,
        'created_user_id' => true,
        'completed' => true,
        'created' => true,
        'modified' => true,
        'media' => true,
        'container' => true,
        'batch' => true,
        'user' => true,
        'CreatedUsers' => true
    ];
}