<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Order Entity
 *
 * @property int $id
 * @property int $customer_id
 * @property \Cake\I18n\FrozenTime $order_date
 * @property \Cake\I18n\FrozenTime $required_date
 * @property \Cake\I18n\FrozenTime|null $shipped_date
 * @property int|null $order_status
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Customer $customer
 * @property \App\Model\Entity\User $user
 */
class Order extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'customer_id' => true,
        'order_date' => true,
        'required_date' => true,
        'shipped_date' => true,
        'order_status' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'customer' => true,
        'user' => true
    ];
}
