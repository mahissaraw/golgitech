<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Rejection Entity
 *
 * @property int $id
 * @property int $type_id
 * @property string $reasons
 * @property float|null $amount
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Type $type
 */
class Rejection extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'type' => true,
        'type_id' => true,
        'reasons' => true,
        'amount' => true,
        'created' => true,
        'modified' => true
    ];
}
