<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PlantCulture Entity
 *
 * @property int $id
 * @property int $media_id
 * @property int $container_id
 * @property int $product_id
 * @property int $media_stock_id
 * @property int $section_id
 * @property int $stage_id
 * @property int $no_of_plants
 * @property string|null $label_code
 * @property \Cake\I18n\FrozenTime|null $is_completed
 * @property int $user_id
 * @property int $operator_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Media $media
 * @property \App\Model\Entity\Container $container
 * @property \App\Model\Entity\Product $product
 * @property \App\Model\Entity\MediaStock $media_stock
 * @property \App\Model\Entity\Section $section
 * @property \App\Model\Entity\Stage $stage
 * @property \App\Model\Entity\User $user
 */
class PlantCulture extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'media_id' => true,
        'container_id' => true,
        'product_id' => true,
        'media_stock_id' => true,
        'section_id' => true,
        'stage_id' => true,
        'no_of_plants' => true,
        'label_code' => true,
        'is_completed' => true,
        'user_id' => true,
        'operator_id' => true,
        'created' => true,
        'modified' => true,
        'media' => true,
        'container' => true,
        'product' => true,
        'media_stock' => true,
        'section' => true,
        'stage' => true,
        'user' => true
    ];
}
